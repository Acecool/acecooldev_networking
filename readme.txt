//
// Networking System - Josh 'Acecool' Moser
//
Includes the basics to run the networking system!

The following exists:
	CL/SV and SH Networking files ( Which handles communication )

	Data Object ( Which is where the data is stored ),

	LRecipientFilter Object ( Lua version of CRecipientFilter with additional feature of being
	able to use it in net messages ),

	UMSG TO NetMSG Mapping System ( Converts UMSG to Net Message without doing anything else
	on your part ),

	Color Constants ( for output ),

	Size Constants ( For determining packets to send, etc ),

	Debug Extension ( for debug output ),

	Global Extension ( With the Message ID function ),

	Table Extension ( with a few helper functions )...

Planned:
	I am going to convert the rest of the UMSG functions over to NetMessage using my net system.
	Additionally, all of the NW Vars will use my net system in an update...


IMPORTANT NOTE: If you are using ULX / ULib with this addon there is a compatibility issue because
	of ULX not using local variables in a file which overwrites the data table. To FIX this issue

	open: addons/ulx/lua/ulx/modules/xgui_server.lua

	Scroll to line 152

	REPLACE:	data = xgui.dataTypes[dtype]

	WITH:		local data = xgui.dataTypes[dtype]

	and that's it, all fixed! Make sure you apply this compatibility fix to your server, local addons
	or anywhere where ULX is installed. I've submitted a pull-request to ULX and the pull request can
	be viewed here: 
		https://github.com/Nayruden/Ulysses/pull/404

	Additionally, I've submitted an ISSUE Report which can be viewed here:
		https://github.com/Nayruden/Ulysses/issues/403
