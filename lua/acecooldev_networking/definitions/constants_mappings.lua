//
//  - Josh 'Acecool' Moser
//


//
// These are defaults to certain flags just in case Set* is called without valid data...
//
DEFAULT_VALUE_MAPPING = {
	// _name Overrides
	TopColor			= Vector( 0.2, 0.5, 1 );
	BottomColor			= Vector( 0.8, 1, 1 );
	SunNormal			= Vector( -0.24, 0.44, 0.87 );
	SunColor			= Vector( 0.2, 0.1, 0 );
	DuskColor			= Vector( 1, 0.2, 0 );
	FadeBias			= 1;
	HDRScale			= 0.66;
	DuskScale			= 1;
	DuskIntensity		= 1;
	SunSize				= 2;
	StarScale			= 0.5;
	StarFade			= 0.5;
	StarSpeed			= 0.01;
	StarTexture			= "skybox/clouds";

	// _type fields
	Angle				= Angle( 0, 0, 0 ); -- Not using angle_zero because of pass by reference...
	Bool				= false;
	Entity				= Entity( 0 );
	Float				= 0;
	Int					= 0;
	String				= "";
	Vector				= Vector( 0, 0, 0 ); -- Not using vector_* because of pass by reference...

	// TYPE_ ENUMS
	[ TYPE_ANGLE ]		= Angle( 0, 0, 0 ); -- Not using angle_zero because of pass by reference...
	[ TYPE_BOOL ]		= false;
	[ TYPE_ENTITY ]		= Entity( 0 ); -- Use world entity instead of empty entity for flag system...
	[ TYPE_NUMBER ]		= 0;
	[ TYPE_STRING ]		= "";
	[ TYPE_VECTOR ]		= Vector( 0, 0, 0 ); -- Not using vector_* because of pass by reference...

	// Unknown Types
	proxy				= nil;
	var					= "undefined_var";

	// Possibly used
	[ TYPE_FUNCTION ]	= function( ) end;
	[ TYPE_NIL ]		= nil;
	[ TYPE_TABLE ]		= { }; -- table
};