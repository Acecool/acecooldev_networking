//
// Flags File ( Isolated to make it easier to find and modify... ) - Josh 'Acecool' Moser
//

// Flags and what they're meant to represent ( categories within the flag system )
FLAG_ACCOUNT		= "account";
FLAG_ADMIN			= "admin";
FLAG_ATTRIBUTES		= "attributes";
FLAG_BANK			= "bank";
FLAG_BANKING		= "bank";
FLAG_CHARACTER		= "character";
FLAG_CURRENCY		= "currency";
FLAG_DATA			= "data";
FLAG_ENVIRONMENT	= "environment";
FLAG_MONEY			= "currency";
FLAG_PLAYER			= "player";
FLAG_REGISTRY		= "registry";
FLAG_STATS			= "stats";
FLAG_VEHICLE		= "vehicle";
FLAG_WEAPON			= "weapon";

// Vehicle
FLAG_VEHICLE_AUDIO	= "vehicle_audio";
FLAG_VEHICLE_SOUNDS	= "vehicle_sounds";
FLAG_VEHICLE_LIGHTS	= "vehicle_lights";

// Other
FLAG_ARREST_INFO	= "info_arrest";

// Networking Flags
FLAG_NWVARS		= "nwvars";
FLAG_GLOBALVARS	= "globalvars";
FLAG_DTVARS		= "dtvars";
FLAG_TABLEVARS	= "tablevars";

// Meant for temporary data, such as logging weapon color when enabling invisibility and using
// physgun so it switches to black, and then back to normal color when non-invisible...
// Or setting stamina so when you log out of admin the stamina is what it was, or ragdoll data...
// This isn't enforced and the temp category isn't purged, it's just another spot...
FLAG_TEMP		= "temp";