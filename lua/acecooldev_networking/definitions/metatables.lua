//
// Meta-Table Loader - Josh 'Acecool' Moser
//

//
// COMMON MetaTables
//
META_PLAYER 							= FindMetaTable( "Player" );
META_ENTITY 							= FindMetaTable( "Entity" );
META_WEAPON 							= FindMetaTable( "Weapon" );
META_VEHICLE 							= FindMetaTable( "Vehicle" );
META_CONVAR 							= FindMetaTable( "ConVar" );


//
// NOT SO COMMON, USE PREFIX
//
META_COLOR 								= FindMetaTable( "Color" );
META_NPC 								= FindMetaTable( "NPC" );
META_IMATERIAL 							= FindMetaTable( "IMaterial" );
META_IRESTORE 							= FindMetaTable( "IRestore" );
META_VECTOR 							= FindMetaTable( "Vector" );
META_ISAVE 								= FindMetaTable( "ISave" );
META_CUSERCMD 							= FindMetaTable( "CUserCmd" );
META_CSOUNDPATCH 						= FindMetaTable( "CSoundPatch" );
META_CMOVEDATA 							= FindMetaTable( "CMoveData" );
META_CEFFECTDATA 						= FindMetaTable( "CEffectData" );
META_ITEXTURE 							= FindMetaTable( "ITexture" );
META_FILE 								= FindMetaTable( "File" );
META_CTAKEDAMAGEINFO 					= FindMetaTable( "CTakeDamageInfo" );
META_VMATRIX 							= FindMetaTable( "VMatrix" );
META_ANGLE 								= FindMetaTable( "Angle" );
META_PHYSOBJ 							= FindMetaTable( "PhysObj" );


//
//
//
if ( SERVER ) then
	META_CLUALOCOMOTION 				= FindMetaTable( "CLuaLocomotion" );
	META_NEXTBOT 						= FindMetaTable( "NextBot" );
	-- META_DATABASE 						= FindMetaTable( "Database" );
	META_PATHFOLLOWER 					= FindMetaTable( "PathFollower" );
	META_CRECIPIENTFILTER 				= FindMetaTable( "CRecipientFilter" );
	META_CNAVAREA 						= FindMetaTable( "CNavArea" );
else
	META_PANEL 							= FindMetaTable( "Panel" );
	META_EMITTER 						= FindMetaTable( "CLuaEmitter" );
	META_PARTICLE 						= FindMetaTable( "CLuaParticle" );
	META_GMODAUDIOCHANNEL 				= FindMetaTable( "IGModAudioChannel" );
end


//
// Interesting...
//
META_LOADLIB 							= FindMetaTable( "_LOADLIB" );
META_PRELOAD 							= FindMetaTable( "_PRELOAD" );
META_LOADED 							= FindMetaTable( "_LOADED" ); // Jit, drive, ai stuff, lots...


//
// Lua / GLua Meta-Tables
//
META_TABLE								= getmetatable( "table" );
META_BOOLEAN							= getmetatable( "boolean" );
META_CTAKEDAMAGEINFO					= getmetatable( "CTakeDamageInfo" );
META_CEFFECTDATA						= getmetatable( "CEffectData" );
META_FUNCTION							= getmetatable( "function" );
META_NUMBER								= getmetatable( "number" );
META_STRING								= getmetatable( "string" );