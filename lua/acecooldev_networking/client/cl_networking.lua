//
// Clientside Networking System - Josh 'Acecool' Moser
//


//
// Adds a console command on the client which gets networked to the server to be executed ( this is so autocomplete will
// work because autocomplete must be clientside, and instead of repeating the same code for every single concommand
// which should be executed on the server but needs an autocomplete system )
//
function networking:AddConCommand( _name, _autocomplete, _help )
	concommand.Add( _name, function( _p, _cmd, _args )
		networking:SendToServer( "concommand.Run", _cmd, _args );
	end, _autocomplete, _help );
end


//
// Helper Function
// Sends data to a server without needing to type nil for the player value if Send
// is called directly.
//
function networking:SendToServer( _name, ... )
	networking:Send( _name, nil, ... );
end


//
//
//
function networking:RequestFlag( _delay, _ent, _flag, _value, _private, _category, _default )
	if ( !networking.__RequestFlag ) then networking.__RequestFlag = { }; end
	if ( !networking.__RequestFlag[ _ent:GetID( ) ] ) then networking.__RequestFlag[ _ent:GetID( ) ] = { }; end
	local _log = networking.__RequestFlag[ _ent:GetID( ) ][ _flag ];

	if ( !_log || !_delay || _delay <= 0 || CurTime( ) - _log > _delay ) then
		// Ask the server for flag data...
		networking:SendToServer( "RequestFlag", _ent, _flag, _value, _private, _category, _default );

		// Keep track of time requested so we can enforce the delay
		networking.__RequestFlag[ _ent:GetID( ) ][ _flag ] = CurTime( ) + _delay;
	end
end


//
// Helper Function - When the client SendToClients another client, the data is routed through a
// pass-through on the server, and then sent to the client. This is the only way this can be done
// but having a helper function do this allows for code to be created quicker which handles trading
// and other functionality.
//
function networking:SendToClient( _name, _client, ... )
	networking:Send( "networking:PassThrough:ClientToClient", nil, _client, _name, ... );
end