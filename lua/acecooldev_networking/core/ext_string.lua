//
//	- Josh 'Acecool' Moser
//


//
// Concatenates two strings without .. ( no rawconcat for meta-tables )
//
function string.concat( self, _text )
	return string.format( "%s%s", ( tostring( self ) || "ErrorConcat" ), _text );
end


//
// Returns only the numbers in a string by stripping non-numbers
//
function string.numbers( _text )
	return tonumber( string.gsub( ( _text || "" ), "%D", "" ), 10 ) || -1;
end


//
// All non alpha-numeric characters become _
//
function string.safe( text )
	text = string.gsub( text, "%W", "_" );
	return text;
end