//
// Extended Print Josh 'Acecool' Moser
//


// BUFFER: If this is too high, there will be missing chunks of data.
PRINT_BUFFER 			= 250;


// Arg separator: Default print function inserts a tab \t for each new argument. Here this can be redefined. Suggestions: tab \t, new line \n, space	, etc..
PRINT_ARG_SEPARATOR 	= "\t";


//
// print( ) default colors are COLOR_CYAN on SERVER, and COLOR_YELLOW/ORANGE on CLIENT
//
PRINT_COLORS = {
	Angle 				= COLOR_CYAN; --
	boolean 			= COLOR_YELLOW; --
	Color 				= nil; -- This actually uses the COLOR of the element, instead of a set color.
	DamageInfo 			= COLOR_RED; --
	CTakeDamageInfo 	= COLOR_GREEN; --
	EffectData 			= COLOR_BLUE;
	CEffectData 		= COLOR_WHITE;	--
	Entity 				= COLOR_WHITE; --
	["function"] 		= COLOR_WHITE; --
	LuaEmitter 			= COLOR_WHITE; --
	LuaParticle 		= COLOR_WHITE; --
	Material 			= COLOR_WHITE; --
	MoveData 			= COLOR_WHITE; --
	number 				= COLOR_WHITE; --
	Panel 				= COLOR_WHITE; --
	Player 				= COLOR_WHITE; --
	RecipientFilter 	= COLOR_WHITE; --
	CRecipientFilter 	= COLOR_WHITE; --
	SoundPatch 			= COLOR_WHITE; --
	String 				= COLOR_WHITE; --
	StringLanguage 		= COLOR_CYAN;
	StringLanguageF 	= COLOR_RED;
	Table 				= COLOR_WHITE; --
	ToString 			= COLOR_GRAY; --
	Texture 			= COLOR_WHITE; --
	UserCmd 			= COLOR_WHITE; --
	Vector 				= COLOR_WHITE; --
	VMatrix 			= COLOR_WHITE; --

	// Other
	ConVar				= COLOR_BLUE;
	Vehicle				= COLOR_CYAN;
	Weapon				= COLOR_ORANGE;
	NPC					= COLOR_YELLOW;
	Restore				= COLOR_SILVER;
	Save				= COLOR_SILVER;
	["file"]			= COLOR_SILVER;
	PhysObj				= COLOR_SILVER;
	NavArea				= COLOR_SILVER;
	PathFollower		= COLOR_SILVER;
	Database			= COLOR_SILVER;
	NextBot				= COLOR_SILVER;
	LuaLocomotion		= COLOR_SILVER;
};


//
// torpint - converts all data-types into strings - Josh 'Acecool' Moser
// Internal function for extended print functionality	can be called like it is to beautify text before saving to a document or so..
//
-- CLuaParticle, CLuaEmitter, CMoveData, CRecipientFilter, CSEnt, CSoundPatch, CTakeDamageInfo, CUserCmd
local function tostringdepth( _depth )
	return string.rep( "\t", _depth );
end


//
// converts input _data to string... Additionally allows tables to be converted to Lua, or other data structures that allow it.
//
function toprint( _data, _lua, _depth, _processed )
	if ( !_depth ) then _depth = 0; end
	if ( !_processed ) then _processed = { }; end

	//
	if ( IsColor( _data ) ) then
		return "Color( " .. _data.r .. ", " .. _data.g .. ", " .. _data.b .. ", " .. _data.a .. " )", PRINT_COLORS.Color || _data;
	elseif ( isstring( _data ) ) then
		// Language system...
		if ( string.StartWith( _data, "#" ) ) then
			local _phrase = string.sub( _data, 2, string.len( _data ) );
			local _lang = language.GetPhrase( _phrase );
			local _color = ( _lang ) && PRINT_COLORS.StringLanguage || PRINT_COLORS.StringLanguageF;
			return tostring( _lang or _phrase ), _color;
		else
			return tostring( _data ), PRINT_COLORS.String;
		end
	elseif ( istable( _data ) ) then
		return table.ToString( _data, _depth, _processed, _lua ), PRINT_COLORS.Table;
	-- elseif ( isfunction( _data ) ) then
		-- return _data, COLOR_WHITE;
	else
		return tostring( _data ), PRINT_COLORS[ type( _data ) ];
	end
end


-- PRINT( )
-- function test( test, testing ) end
-- print( debug.getinfo( test ), debug.getparams( test ) );
-- local _colorOut = ( PRINT_COLORS[ type( _data ) ] && "COLOR: " .. PRINT_COLORS[ type( _data ) ].r .. " " .. PRINT_COLORS[ type( _data ) ].g .. " " .. PRINT_COLORS[ type( _data ) ].b .. " " .. PRINT_COLORS[ type( _data ) ].a || "NO COLOR" );
-- MsgC( COLOR_CYAN, "TYPE: " .. type( _data ) .. _colorOut .. "\n" )
-- local _depth = "";
-- for i = 1, _depth or 1 do
	-- _depth = _depth .. "\t";
-- end
-- return _depth;

// Debug
-- if ( IsTable( _data ) ) then _data = table.sortnonnumerickey( _data ); end


//
//
//
if ( !PRINT ) then PRINT = print; end


//
//
//
function print( ... )
	local _args = { ... };

	// Each arg...
	for k, v in pairs( _args ) do
		// Convert data to friendly output...
		local _p, _c = toprint( v );

		// Color system, needs to be an arg for this to work.
		local _color = _c || COLOR_SNOW // / COLOR_SILVER

		// Buffer / Chunking system.
		local _len = string.len( _p );
		local _count = math.ceil( _len / PRINT_BUFFER );
		for i = 1, _count do
			local _sub = string.sub( _p, 1 + ( PRINT_BUFFER * i ) - PRINT_BUFFER, PRINT_BUFFER * i );

			// This is where we pass printing off to, so we can do it in color!
			MsgC( _color || color_white, _sub || "" );
		end

		// End of the arg, add the separator.
		MsgC( _c || color_white, PRINT_ARG_SEPARATOR );
	end

	// If the separator is not a new line, we need a new line to end it all or the next print or MsgC will be on the same line!
	if ( !( PRINT_ARG_SEPARATOR == "\n" ) ) then
		PRINT( );
	end
end


//
// GenerateExample for print compatibility - Josh 'Acecool' Moser
//
function print_example( )
	local _oldSeparator = PRINT_ARG_SEPARATOR;
	PRINT_ARG_SEPARATOR = "\n";
	language.Add( "Test", "Testing" );

	local function test( )

	end

	print(
		Angle( 0, 0, 0 ), //PRINT_ARG_SEPARATOR,
		true, //PRINT_ARG_SEPARATOR,
		false, //PRINT_ARG_SEPARATOR,
		Color( 255, 0, 0, 255 ), //PRINT_ARG_SEPARATOR,
		Color( 0, 255, 0, 255 ), //PRINT_ARG_SEPARATOR,
		Color( 0, 0, 255, 255 ), //PRINT_ARG_SEPARATOR,
		DamageInfo( ), //PRINT_ARG_SEPARATOR,
		EffectData( ), //PRINT_ARG_SEPARATOR,
		NULL, //PRINT_ARG_SEPARATOR,
		print, //PRINT_ARG_SEPARATOR,
		( CLIENT && ParticleEmitter( vector_origin ) || "Server Doesn't Use ParticleEmitter( )" ), //PRINT_ARG_SEPARATOR,
		"No Example for Particles", //PRINT_ARG_SEPARATOR,
		( CLIENT && draw.GetMaterial( "vgui/gradient_down.vmt" ) || "Server Doesn't Use Materials!" ), //PRINT_ARG_SEPARATOR,
		"No Example for MoveData", //PRINT_ARG_SEPARATOR,
		1337, //PRINT_ARG_SEPARATOR,
		( CLIENT && vgui.GetWorldPanel( ) || "Server Doesn't Use Panels!" ), //PRINT_ARG_SEPARATOR,
		Entity( 1 ), //PRINT_ARG_SEPARATOR,
		( CLIENT && "Client doesn't use RecipientFilter( )" || RecipientFilter( ) ), //PRINT_ARG_SEPARATOR,
		"No example for SoundPatch", //PRINT_ARG_SEPARATOR,
		language.GetPhrase( "Test" ), //PRINT_ARG_SEPARATOR,
		language.GetPhrase( "ThisDoesntExist" ), //PRINT_ARG_SEPARATOR,
		{ "Table", "Example", { "With", { "Unlimited", { "Nesting!" } } } }, //PRINT_ARG_SEPARATOR,
		"String", //PRINT_ARG_SEPARATOR,
		( CLIENT && "No Example for Textures" || "Server Doesn't Use Textures!" ), //PRINT_ARG_SEPARATOR,
		"No Example for UserCmd", //PRINT_ARG_SEPARATOR,
		Vector( 0, 0, 0 ), //PRINT_ARG_SEPARATOR,
		Matrix( ), //PRINT_ARG_SEPARATOR
		test
	);
	PRINT_ARG_SEPARATOR = _oldSeparator;
end

concommand.Add( "dev_printexample", function( _ply, _cmd, _args )
	print_example( );
	-- MsgC( COLOR_RED, RecipientFilter( ) );
end );