//
// Debug Extension - Josh 'Acecool' Moser
//


//
//
//
debug = debug || { };


//
// The table which holds the debugging types
//
debug.__allowed_output = debug.__allowed_output || { };


//
//
//
function debug.add( _type, ... )
	// Recursive call for additional arguments added
	if ( #{ ... } > 0 ) then debug.add( ... ); end

	// Add the debugger to the list...
	debug.__allowed_output[ _type ] = true;
end


//
// If we want to remove one from the list
//
function debug.remove( _type )
	debug.__allowed_output[ _type ] = nil;
end


//
// To clear all of the types from the list
//
function debug.removeall( _type )
	debug.__allowed_output = { };
end


//
// This is how it outputs the text...
//
function debug.print( _type, ... )
	if ( !DEBUG && !debug.__allowed_output[ _type ] ) then return; end

	local _tab = {
		COLOR_RED, "[ ";
		COLOR_RED, "Debugging ";
		COLOR_CYAN, ( GM || GAMEMODE ).Name;
		COLOR_RED, " ] ";
		COLOR_WHITE, _type;
		COLOR_RED, ": ";
		-- COLOR_WHITE,	.. "\n";
	};
	MsgC( unpack( _tab ) );
	print( ... );
end


//
// Data Class Debugging Text
//
--[[
// Add many to all at once.. You can group specific ones together if need-be
debug.add(
	// Accessor Function Building...
	"Data:BuildAccessors",

	// Initializing Output
	"Data:InitializePublic", "Data:InitializePrivate",

	// Getting Flag Output
	"Data:GetDefault", "Data:GetPublic", "Data:GetPrivate", "Data:RequestPrivate",

	// Setting Flag Output
	"Data:SetFlag", "Data:SetPublicFlag", "Data:SetPrivateFlag", "Data:SetPrivateLinkedFlag",

	// Error Output
	"Data:GetEntityIDError", "Data:SetFlagNoChangeError",

	// Reset Output
	"Data:ResetFlags", "Data:ResetFlagsError",

	// Linking Output
	"Data:UpdateLink", "Data:LinkData", "Data:UnLinkData",

	// Receiver Output
	"Data:RcvEntitySetFlag", "Data:RcvEntityResetFlags", "Data:RcvSyncPData", "Data:RcvSyncData", "Data:RcvUpdateDataLink"
); --]]

// Or 1 by 1..
-- debug.add( "Data:BuildAccessors" );
-- debug.add( "Data:InitializePublic" );
-- debug.add( "Data:InitializePrivate" );
-- debug.add( "Data:GetDefault" );
-- debug.add( "Data:GetPublic" );
-- debug.add( "Data:GetPrivate" );
-- debug.add( "Data:RequestPrivate" );
-- debug.add( "Data:SetFlag" );
-- debug.add( "Data:SetPublicFlag" );
-- debug.add( "Data:SetPrivateFlag" );
-- debug.add( "Data:SetPrivateLinkedFlag" );
-- debug.add( "Data:GetEntityIDError" );
-- debug.add( "Data:SetFlagNoChangeError" );
-- debug.add( "Data:ResetFlags" );
-- debug.add( "Data:ResetFlagsError" );
-- debug.add( "Data:UpdateLink" );
-- debug.add( "Data:LinkData" );
-- debug.add( "Data:UnLinkData" );
-- debug.add( "Data:RcvEntitySetFlag" );
-- debug.add( "Data:RcvEntityResetFlags" );
-- debug.add( "Data:RcvSyncPData" );
-- debug.add( "Data:RcvSyncData" );
-- debug.add( "Data:RcvUpdateDataLink" );


-- debug.add( "Networking:UnhandledCall" );
-- debug.add( "Networking:UnhandledMessage" );
-- debug.add( "Networking:ClientUnhandledMessage" );
-- debug.add( "Networking:AddReceiver" );
-- debug.add( "Networking:ActivateThink" );
-- debug.add( "Networking:DeactivateThink" );
-- debug.add( "Networking:Think" );
-- debug.add( "Networking:WaitingOnPackets" );
-- debug.add( "Networking:IgnoringUnconnectedPlayer" );
-- debug.add( "Networking:SendingPackets" );
-- debug.add( "Networking:" );
-- debug.add( "Networking:" );
-- debug.add( "Networking:" );

-- debug.add( "managed_list:ConnectedPlayers" );