//
//	- Josh 'Acecool' Moser
//
if ( SERVER ) then
	//
	// Helper-Function to ensure users don't bypass client-code...
	//
	local function DetectPlayerFullyConnected( _p, _hook, ... )
		if ( !IsValid( _p ) || !_p.IsFullyConnected || !_p.GetFlag || _p:IsFullyConnected( ) ) then return; end

		// Set the name, manages name changes.
		_p:SetFlag( "name", _p:Nick( ), true, FLAG_PLAYER );
		_p:SetFlag( "timeconnected", CurTime( ), true, FLAG_PLAYER );
		_p:SetFlag( "isfullyconnected", true, false, FLAG_PLAYER );

		// Notify the client that they're here. This'll be incorporated with the net system integration update.
		networking:CallHook( "PlayerFullyConnected", _p, "GAMEMODE", _p );

		// Broadcast the hook...
		hook.Call( "PlayerFullyConnected", GAMEMODE, _p );

		// Sync Data ( Perfect place to call it )...
		hook.Call( "SyncData", GAMEMODE, _p );
	end


	//
	// Security feature to ensure client doesn't bypass code.
	//
	hook.Add( "SetupMove", "PlayerFullyConnected:SetupMove", function( _p, _move, _cmd )
		DetectPlayerFullyConnected( _p, "SetupMove", _p, _move, _cmd );
	end );

	hook.Add( "Initialize", "AcecoolDevNetworking:ServersideGM", function( )
		//
		// SyncData - Override for syncing data to the client after they're verified fully-connected - Josh 'Acecool' Moser
		//
		function GAMEMODE:SyncData( _p )
			// Send Syncable Entity Flags to Clients
			networking:SendToClient( "SyncData", _p, data:GetFlags( ) );
		end


		//
		// PlayerFullyConnected Hook - Josh 'Acecool' Moser
		//
		// Note: This can be separated into a cl_ and sv_ file if you wish to keep them separate,
		// and keep server code from being downloaded by the client.
		//
		function GAMEMODE:PlayerFullyConnected( _p )
			// Notify in console when the player has fully connected
			MsgC( COLOR_WHITE, tostring( _p ) .. " has fully connected. ( " .. CurTime( ) .. " )\n" );
		end
	end );
else
	hook.Add( "Initialize", "AcecoolDevNetworking:ClientsideGM", function( )
		//
		// PlayerFullyConnected Hook - Josh 'Acecool' Moser
		//
		// Note: This can be separated into a cl_ and sv_ file if you wish to keep them separate,
		// and keep server code from being downloaded by the client.
		//
		function GAMEMODE:PlayerFullyConnected( _p )
			hook.Call( "SetupClientMap", GAMEMODE, _p );
		end
	end );


	//
	// Allow the server to network hooks without adding a specific receiver for each one...
	//
	networking:AddReceiver( "CallHook", function( _lp, _name, _table, ... )
		local _upper = string.upper( _table )
		local _hooks = hook.GetTable( );
		local _bHookExists = _hooks[ _name ];
		local _bGMHookExists = ( ( !_table || _upper == "GM" || _upper == "GAMEMODE" ) && _G[ _upper ][ _name ] );
		local _bCustomHookExists = ( _table && _G[ _table ] && _G[ _table ][ _name ] );

		if ( !_bHookExists && !_bGMHookExists && !_bCustomHookExists ) then
			networking:SendToServer( "Error", "Hook Not Found", _name, _table );
		else
			hook.Call( _name, ( ( _table ) && _G[ _table ] || nil ), unpack( { ... } ) );
		end
	end );
end