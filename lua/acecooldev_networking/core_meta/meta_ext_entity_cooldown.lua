//
// Entity Extension - Josh 'Acecool' Moser
//


//
// CoolDown Vars
//
local FLAG_COOLDOWN = "cooldowns"

// If we want the new value to overwrite the old, only if new time is more
COOLDOWN_OVERWRITE_MORE = 0; -- Default action..

// If the cooldown system should accumulate time / add it to the existing time
COOLDOWN_ADD				= 1;
COOLDOWN_ACCUMULATE			= COOLDOWN_ADD;

// If we want to prevent a new cooldown from being applied until the existing one is gone
COOLDOWN_SKIP				= 2;
COOLDOWN_PREVENT			= COOLDOWN_SKIP;
COOLDOWN_PREVENT_OVERWRITE	= COOLDOWN_SKIP;
COOLDOWN_EXPIRE_FIRST		= COOLDOWN_SKIP;

// If we want the new value to overwrite the old, even if less time
COOLDOWN_ALWAYS_OVERWRITE	= 3;
COOLDOWN_OVERWRITE_ALWAYS	= COOLDOWN_ALWAYS_OVERWRITE;
COOLDOWN_OVERWRITE			= COOLDOWN_ALWAYS_OVERWRITE;

// If we want the new value to overwrite the old, only if less time
COOLDOWN_OVERWRITE_LESS		= 4;


//
// Set Cooldown for how many seconds an entity must wait before using something... Uses GetFlag / SetFlag
//
function META_ENTITY:SetCooldown( _name, _expiry, _action, _local )
	// While all strings can be used, we'll change them slightly by making them all lowercase
	_name = string.lower( _name );

	// Grab the current cooldown to see if one already exists...
	local _cooldown, _timeleft = self:HasCooldown( _name, 4, _local );

	// Determine action to take if we have a pre-existing cooldown
	if ( _cooldown ) then
		// Default Action... Always use the greater value...
		if ( !_action || _action == COOLDOWN_OVERWRITE_MORE ) then
			// Overwrite with the greater value
			_expiry = math.Max( _expiry, _timeleft );
		elseif ( !_action || _action == COOLDOWN_OVERWRITE_LESS ) then
			// Overwrite with the lesser value
			_expiry = math.Min( _expiry, _timeleft );
		elseif ( _action == COOLDOWN_ADD ) then
			// Accumulate Values
			_expiry = _expiry + _timeleft;
		elseif ( _action == COOLDOWN_SKIP ) then
			// Prevent the cooldown from being updated until it expires! Return false just in case you do if ( _ent:SetCooldown( ...
			return false;
		end
	end

	// Set the cooldown / update it...
	if ( _local ) then
		self.__Cooldowns[ _name ] = CurTime( ) + _expiry;
	else
		self:SetFlag( _name, CurTime( ) + _expiry, true, FLAG_COOLDOWN );
	end

	// Return true just in case you do if ( _ent:SetCooldown( ...
	return true;
end


//
// Alias ( Uses a localized table; useful if we don't want it networked or the time it takes to network takes too long )
//
function META_ENTITY:HasLocalCooldown( _name, _decimals )
	local _cooldown, _timeleft = self:HasCooldown( _name, _decimals, true );
	return _cooldown, _timeleft;
end


//
// Alias ( Uses a localized table; useful if we don't want it networked or the time it takes to network takes too long )
//
function META_ENTITY:SetLocalCooldown( _name, _expiry, _action )
	return self:SetCooldown( _name, _expiry, _action, true );
end//
// CoolDown Vars
//
local FLAG_COOLDOWN			= "cooldowns";

// If we want the new value to overwrite the old, only if new time is more
COOLDOWN_OVERWRITE_MORE		= 0; -- Default action..

// If the cooldown system should accumulate time / add it to the existing time
COOLDOWN_ADD				= 1;
COOLDOWN_ACCUMULATE			= COOLDOWN_ADD;

// If we want to prevent a new cooldown from being applied until the existing one is gone
COOLDOWN_SKIP				= 2;
COOLDOWN_PREVENT			= COOLDOWN_SKIP;
COOLDOWN_PREVENT_OVERWRITE	= COOLDOWN_SKIP;
COOLDOWN_EXPIRE_FIRST		= COOLDOWN_SKIP;

// If we want the new value to overwrite the old, even if less time
COOLDOWN_ALWAYS_OVERWRITE	= 3;
COOLDOWN_OVERWRITE_ALWAYS	= COOLDOWN_ALWAYS_OVERWRITE;
COOLDOWN_OVERWRITE			= COOLDOWN_ALWAYS_OVERWRITE;

// If we want the new value to overwrite the old, only if less time
COOLDOWN_OVERWRITE_LESS		= 4;


//
// Check to see if entity must wait before using something... Uses GetFlag
//
function META_ENTITY:HasCooldown( _name, _decimals, _local )
	// Initialize the cooldown table ( if we're using local version )
	if ( _local && !self.__Cooldowns ) then self.__Cooldowns = { }; end

	// While all strings can be used, we'll change them slightly by making them all lowercase
	_name = string.lower( _name );

	// Check to see if the specific cooldown exists or use 0
	local _cooldown = ( _local ) && self.__Cooldowns[ _name ] || self:GetFlag( _name, 0, true, FLAG_COOLDOWN );

	// How much time is left - since we use CurTime( ) + _expiry we reverse the subtraction.
	// If we have positive number, then there is time left.
	local _rawtimeleft = _cooldown - CurTime( );

	// Never let time-left go below 0 ( math.Max ), Round it to decimal count if given, otherwise no decimals
	local _timeleft = math.Round( math.Max( _rawtimeleft, 0 ), ( _decimals || 0 ) );

	// Return true/false, and modified time-left
	return ( _rawtimeleft > 0 ), _timeleft;
end


//
// Set Cooldown for how many seconds an entity must wait before using something... Uses GetFlag / SetFlag
//
function META_ENTITY:SetCooldown( _name, _expiry, _action, _local )
	// While all strings can be used, we'll change them slightly by making them all lowercase
	_name = string.lower( _name );

	// Grab the current cooldown to see if one already exists...
	local _cooldown, _timeleft = self:HasCooldown( _name, 4, _local );

	// Determine action to take if we have a pre-existing cooldown
	if ( _cooldown ) then
		// Default Action... Always use the greater value...
		if ( !_action || _action == COOLDOWN_OVERWRITE_MORE ) then
			// Overwrite with the greater value
			_expiry = math.Max( _expiry, _timeleft );
		elseif ( !_action || _action == COOLDOWN_OVERWRITE_LESS ) then
			// Overwrite with the lesser value
			_expiry = math.Min( _expiry, _timeleft );
		elseif ( _action == COOLDOWN_ADD ) then
			// Accumulate Values
			_expiry = _expiry + _timeleft;
		elseif ( _action == COOLDOWN_SKIP ) then
			// Prevent the cooldown from being updated until it expires! Return false just in case you do if ( _ent:SetCooldown( ...
			return false;
		end
	end

	// Set the cooldown / update it...
	if ( _local ) then
		self.__Cooldowns[ _name ] = CurTime( ) + _expiry;
	else
		self:SetFlag( _name, CurTime( ) + _expiry, true, FLAG_COOLDOWN );
	end

	// Return true just in case you do if ( _ent:SetCooldown( ...
	return true;
end


//
// Alias ( Uses a localized table; useful if we don't want it networked or the time it takes to network takes too long )
//
function META_ENTITY:HasLocalCooldown( _name, _decimals )
	local _cooldown, _timeleft = self:HasCooldown( _name, _decimals, true );
	return _cooldown, _timeleft;
end


//
// Alias ( Uses a localized table; useful if we don't want it networked or the time it takes to network takes too long )
//
function META_ENTITY:SetLocalCooldown( _name, _expiry, _action )
	return self:SetCooldown( _name, _expiry, _action, true );
end