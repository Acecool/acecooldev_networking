//
// Returns an ID that will always be the same, regardless of max-players set on server - Josh 'Acecool' Moser
//
// NOTE: If you want to use this, and use it well, read this: https://dl.dropboxusercontent.com/u/26074909/tutoring/entities/entity_getid_mapping_system.lua.html
//
function META_ENTITY:GetID( )
	local _index = ( ( self:EntIndex( ) + 128 ) - game.MaxPlayers( ) );

	// Prevents World Data from being removed by "physics_entity_solver" reporting 0.
	if ( IsValid( self ) && _index == 0 && self != game.GetWorld( ) ) then
		-- Log( self, self:GetClass( ) .. " is posing as world entity 0" );
		_index = nil;
	end

	return _index || -1;
end


//
// Front-End for Flags - Resets flags / Nullifies flags - Josh 'Acecool' Moser
//
function META_ENTITY:ResetFlags( )
	if ( !data || !istable( data ) ) then MsgC( Color( 255, 0, 0, 255 ), "Class->data doesn't exist!" ); return; end

	local _flags = data:ResetFlags( self:GetID( ) );
	-- local _pflags = data:ResetFlags( self:GetID( ), true );

	return _flags, _pflags;
end


//
// Front-End for Flags - Gets value of a flag - Josh 'Acecool' Moser
//
function META_ENTITY:GetFlag( _flag, _default, _private, _category, _request )
	if ( !data || !istable( data ) || !data.GetFlag ) then MsgC( Color( 255, 0, 0, 255 ), "Class->data doesn't exist!" ); return; end

	return data:GetFlag( self, _flag, _default, _private, _category, _request );
end


//
// Front-End for Flags - Sets flag on this entity - Josh 'Acecool' Moser
//
function META_ENTITY:SetFlag( _flag, _value, _private, _category )
	if ( !data || !istable( data ) || !data.GetFlag ) then MsgC( Color( 255, 0, 0, 255 ), "Class->data doesn't exist!" ); return; end

	return data:SetFlag( self, _flag, _value, _private, _category );
end


//
// Return if a player is fully connected - Josh 'Acecool' Moser
//
function META_ENTITY:IsFullyConnected( )
	if ( !self.GetFlag ) then MsgC( Color( 255, 0, 0, 255 ), "Player->GetFlag data doesn't exist!" ); return; end
	return self:GetFlag( "isfullyconnected", false, false, FLAG_PLAYER );
end