//
//
//
util.AddNetworkString( NETWORKING_MESSAGE_NAME );

-- util.AddNetworkString( "__NETWORKING_CHUNKS_TO_SV__" );
-- util.AddNetworkString( "__NETWORKING_CHUNKS_TO_CL__" );

player.__ConnectedCount = player.__ConnectedCount || 0;

//
//
//
function player.GetAllConnected( )
	return player.__ConnectedPlayers || { };
end


//
//
//
function player.GetConnectedCount( )
	return player.__ConnectedCount;
end


//
//
//
hook.Add( "PlayerFullyConnected", "AcecoolNetworking:PlayerManagedList:OnConnect", function( _p )
	MsgC( COLOR_GREEN, _p .. " added to managed player connected list.\n" );

	if ( !player.__ConnectedPlayers ) then player.__ConnectedPlayers = { }; end
	if ( !player.__ConnectedPlayersKeys ) then player.__ConnectedPlayersKeys = { }; end
	local _key = table.insert( player.__ConnectedPlayers, _p );
	player.__ConnectedPlayersKeys[ _p:SteamID( ) ] = _key;
	player.__ConnectedCount = player.__ConnectedCount + 1;
end );


//
//
//
gameevent.Listen( "player_disconnect" );
hook.Add( "player_disconnect", "AcecoolNetworking:PlayerManagedList:OnDisconnect", function( _data )
	local _name = _data.name;			// Same as Player:Nick( );
	local _steamid = _data.networkid;	// Same as Player:SteamID( );

	MsgC( COLOR_RED, "Player " .. _name .. " [ " .. _steamid .. " ] removed from managed player connected list.\n" );

	if ( player.__ConnectedPlayersKeys ) then
		local _key = player.__ConnectedPlayersKeys[ _steamid ];
		table.remove( player.__ConnectedPlayers, _key );
		player.__ConnectedCount = player.__ConnectedCount - 1;
	end
end );


//
// Registers a safe flag the client may sync to the server for themselves
// @args: String _name, ENUM _type, T _values, function _shouldsync ( return non-nil to reject sync ), function _callback ( on success )
// @return: nil
//
function networking:RegisterSyncableFlag( _name, _datatype, _values, _private, _category, _shouldsync, _callback )
	local _flags = networking[ NET_SHARED ].syncable_flags;

	_flags[ _name ] = { };
	_flags[ _name ].private = ( _private ) && _private || nil;
	_flags[ _name ].category = ( _category ) && _category || nil;
	_flags[ _name ].shouldsync = _shouldsync;
	_flags[ _name ].callback = _callback;
	

	if ( !_datatype && !( _values || _values == false ) ) then
		MsgC( COLOR_RED, "SyncableFlag Registry [ ", COLOR_WHITE, _name, COLOR_RED, " ] WARNING - ", COLOR_WHITE, "Leaving BOTH authorized Data-Type & Value fields blank creates a security risk!\n" );
		return true;
	end

	//
	local _types = 0;
	if ( _datatype ) then
		_flags[ _name ].types = { };

		if ( istable( _datatype ) ) then
			for k, v in pairs( _datatype ) do
				// REPEAT
				_flags[ _name ].types[ v ] = true;
				_types = _types + 1;
				// REPEAT
			end
		else
			// REPEAT
			_flags[ _name ].types[ _datatype ] = true;
			_types = _types + 1;
			// REPEAT
		end
	end

	//
	if ( _values || _values == false ) then
		_flags[ _name ].values = { };

		if ( istable( _values ) ) then
			for k, v in pairs( _values ) do
				// REPEAT
				local _type = type( v );
				local _typeid = TypeID( v );

				// If no registered data-types, then allow the values... Otherwise, make sure the value works with the type
				if ( _types == 0 || _flags[ _name ].types[ _type ] || _flags[ _name ].types[ _typeid ] ) then
					_flags[ _name ].values[ v ] = true;
				else
					MsgC( COLOR_RED, "SyncableFlag Registry [ ", COLOR_WHITE, _name, COLOR_RED, " ] WARNING - ", COLOR_WHITE, "value \"" .. v .. "\" isn't acceptable for selected input data-types!\n" );
				end
				// REPEAT
			end
		else
			// REPEAT
			local _type = type( _values );
			local _typeid = TypeID( _values );
			if ( _types == 0 || _flags[ _name ].types[ _type ] || _flags[ _name ].types[ _typeid ] ) then
				_flags[ _name ].values[ _values ] = true;
			else
				print( "SyncableFlag Registry [ " .. _name .. " ] Value: ", _values, "isn't acceptable!" );
			end
			// REPEAT
		end
	end
end


//
//
//
function networking:CanSyncFlag( _name, _value )
	local _flags = networking[ NET_SHARED ].syncable_flags;

end


//
// Helper Function
// Sends data to one specific client; it's exactly the same argument layout as using Send directly.
//
-- print( "SendToClient > ", _client, _table )
function networking:SendToClient( _name, _client, ... )
	networking:Send( _name, _client, ... );
end


//
// Helper Function
// Send data to all players without the need to enter client infomation
//
function networking:Broadcast( _name, ... )
	networking:Send( _name, nil, ... );
end


//
// Helper Function
// Send data to all players, excluding one or more
//
function networking:BroadcastExcluding( _name, _client, ... )
	local _playerList = { };
	local _bClients = istable( _client );

	for k, v in pairs( player.GetAllConnected( ) ) do
		if ( ( _bClients && !table.HasValue( _client, v ) ) || ( v != _client ) ) then
			table.insert( _playerList, v );
		end
	end

	if ( #_playerList > 0 ) then
		networking:Send( _name, _playerList, ... );
	end
end


//
// Helper Function
// Creates a list of players to send data to, who are in the _radius of _client.
//
function networking:BroadcastInRadius( _name, _client, _radius, ... )
	local _playerList = { };
	for k, v in pairs( player.GetAllConnected( ) ) do
		if ( _client:GetPos( ):Distance( v:GetPos( ) ) <= _radius ) then
			table.insert( _playerList, v );
		end
	end

	if ( #_playerList > 0 ) then
		networking:Send( _name, _playerList, ... );
	end
end


//
// Helper Function
// Creates a list of players to send data to who are admins and sends to them.
//
function networking:BroadcastToAdmins( _name, ... )
	local _adminList = { };
	for k, v in pairs( player.GetAllConnected( ) ) do
		if ( v:IsAdmin( ) ) then
			table.insert( _adminList, v );
		end
	end

	if ( #_adminList > 0 ) then
		networking:Send( _name, _adminList, ... );
		return true;
	else
		return false;
	end
end


//
// Helper function to call a hook on one to many clients...
//
function networking:CallHook( _name, _p, _table, ... )
	networking:SendToClient( "CallHook", _p, _name, _table, ... );
end


//
// Helper function to broadcast a hook.Call to clients...
//
function networking:BroadcastHook( _name, _table, ... )
	networking:Broadcast( "CallHook", _name, _table, ... );
end