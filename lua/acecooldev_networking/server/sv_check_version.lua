//
// Compare the current version vs local version - Josh 'Acecool' Moser
// NOTE: Will alert to differing levels of version updates; there are 3 "types"
// 		 Major > Medium > Builds = 0.0.0 = Major.Medium.Build
//
// Each segment may continue increasing in value, but if a parent increases then all
// 	lower values are reset to 0.. ie: 0.0.49 = 49 small changes / updates occurred,
// 	then we add a "medium" level update, 49 is reset, and 1 added: 0.1.0.. The build
// 	counter resets each medium level update. Likewise, Major updates will set medium
// 	and builds to 0... There'll be a big segment on how versions are handled. For the
// 	meantime I'll mostly be using build and medium levels until we "launch"; by launch
// 	I am talking about when this skeletonized dev-base is at a point where most systems
// 	are implemented such as round system ( to convert TTT, Jailbreak, etc ), and other
// 	major systems in place.
//
// Build updates are minor. They may add new hooks, functionality, systems, language
// 	additions, etc. These are generally considered "Maintenance" level updates.
//
// Medium updates are in the middle. These updates can bring new features too, but
// 	can also be security fixes, feature completion, etc... Medium and higher should
// 	generally not be avoided.
//
// Major updates usually represent a large stepping stone, system overhauls, rewrites,
// 	or other "large-scale" type updates.
//


//
// Vars
//
VERSION_HTTP_PATH 		= "https://bitbucket.org/Acecool/acecooldev_networking/raw/master/version.txt";

VERSION_LOCAL_MAJOR 	= VERSION_LOCAL_MAJOR		|| 0;
VERSION_LOCAL_MEDIUM 	= VERSION_LOCAL_MEDIUM		|| 0;
VERSION_LOCAL_BUILD 	= VERSION_LOCAL_BUILD		|| 0;
VERSION_LOCAL			= VERSION_LOCAL				|| "0.0.0";

VERSION_OFFICIAL_MAJOR 	= VERSION_OFFICIAL_MAJOR	|| 0;
VERSION_OFFICIAL_MEDIUM = VERSION_OFFICIAL_MEDIUM	|| 0;
VERSION_OFFICIAL_BUILD 	= VERSION_OFFICIAL_BUILD	|| 0;
VERSION_OFFICIAL 		= VERSION_OFFICIAL			|| "0.0.0";


//
//
//
hook.Add( "Initialize", "AcecoolDevNetworking:VersionControl", function( )
	//
	//
	//
	function GAMEMODE:CheckVersion( _p )
		local _func = ( !_p || _p == NULL || !IsValid( _p ) ) && MsgC || function( _p, _msg ) _p:PrintMessage( HUD_PRINTTALK, _msg ); end;
		local _funcarg = ( !_p || _p == NULL || !IsValid( _p ) ) && Color( 0, 255, 255, 255 ) || _p;

		//
		local _folder = "addons/acecooldev_networking/";
		if ( !file.Exists( _folder .. "version.txt", "GAME" ) ) then
			MsgC( Color( 255, 0, 0, 255 ), "version.txt is missing from " .. _folder .. "!\nRename this networking addon directory to acecooldev_networking" );
			return false;
		end

		//
		local _data = file.Read( _folder .. "version.txt", "GAME" );
		local _version = string.Explode( ".", _data );
		VERSION_LOCAL_MAJOR 	= _version[ 1 ];
		VERSION_LOCAL_MEDIUM 	= _version[ 2 ];
		VERSION_LOCAL_BUILD 	= _version[ 3 ];
		VERSION_LOCAL = VERSION_LOCAL_MAJOR .. "." .. VERSION_LOCAL_MEDIUM .. "." .. VERSION_LOCAL_BUILD;

		//
		VERSION_OFFICIAL = VERSION_OFFICIAL_MAJOR .. "." .. VERSION_OFFICIAL_MEDIUM .. "." .. VERSION_OFFICIAL_BUILD;

		http.Fetch( VERSION_HTTP_PATH,
			function( _body, _len, _headers, _source )
				local _explode = string.Explode( ".", _body );
				VERSION_MAJOR 	= _explode[ 1 ];
				VERSION_MEDIUM 	= _explode[ 2 ];
				VERSION_BUILD 	= _explode[ 3 ];
				VERSION_OFFICIAL = _body;

				if ( VERSION_LOCAL == VERSION_OFFICIAL ) then
					_func( ( ( IsValid( _p ) ) && _funcarg || COLOR_GREEN ), "SERVER IS UP TO DATE! Running Version: " .. VERSION_OFFICIAL .. "\n" );
				else
					_func( ( ( IsValid( _p ) ) && _funcarg || COLOR_RED ), "SERVER IS OUT OF DATE! Running Version: " .. VERSION_LOCAL .. " < " .. VERSION_OFFICIAL .. "\n" );
				end
			end,
			function( _err )
				_func( ( ( IsValid( _p ) ) && _funcarg || COLOR_RED ), "Error fetching version information. Retrying in 5 minutes!\n" );
				timer.Simple( TIME_MINUTE * 5, function( )
					hook.Call( "CheckVersion", GAMEMODE );
				end );
			end
		 );
	end


	timer.Simple( 5, function( )
		hook.Call( "CheckVersion", GAMEMODE );
	end );
end );


//
//
//
hook.Add( "PlayerFullyConnected", "AcecoolDevNetworking:VersionControl", function( _p )
	if ( VERSION_OFFICIAL == "0.0.0" ) then return; end
	local _output = ( VERSION_LOCAL == VERSION_OFFICIAL ) && "Server is up to date!" || "Server requires updates!";

	_p:PrintMessage( HUD_PRINTTALK, "Official Version: " .. VERSION_OFFICIAL );
	_p:PrintMessage( HUD_PRINTTALK, "Local Version: " .. VERSION_LOCAL );
	_p:PrintMessage( HUD_PRINTTALK, _output );
end );


//
//
//
concommand.Add( "dev_version", function( _p, _cmd, _args )
	if ( _p != NULL && !_p:IsAdmin( ) ) then return; end

	hook.Call( "CheckVersion", GAMEMODE, _p );
end );