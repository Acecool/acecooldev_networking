//
// Data Storage System - Josh 'Acecool' Moser
// This ties heavily into the networking system. This handles all of the data...
//
if ( !data ) then
	data = { };
	data.__index = data;

	// Entity and World Data
	data.__flags = {
		public = { };
		private = { };
		links = { };
	};

	// Managed Lists
	data.__managed_lists = {
		connected_players = { };
		spawned_vehicles = { };
	};
end
data.__accessors_log = "";

local _short = true;
data.__debug_tables = {
	Player				= ( _short ) && "_p"			|| "META_PLAYER";
	Weapon				= ( _short ) && "_w"			|| "META_WEAPON";
	Entity				= ( _short ) && "_ent"			|| "META_ENTITY";
	Vehicle				= ( _short ) && "_v"			|| "META_VEHICLE";
	ConVar				= ( _short ) && "_convar"		|| "META_CONVAR";
	-- //Color			= ( _short ) && "_col"			|| "META_COLOR";
	NPC					= ( _short ) && "_npc"			|| "META_NPC";
	IMaterial			= ( _short ) && "_mat"			|| "META_IMATERIAL";
	IRestore			= ( _short ) && "_restore"		|| "META_IRESTORE";
	Vector				= ( _short ) && "_pos"			|| "META_VECTOR";
	ISave				= ( _short ) && "_save"			|| "META_ISAVE";
	CUserCmd			= ( _short ) && "_cmd"			|| "META_CUSERCMD";
	CSoundPatch			= ( _short ) && "_sound"		|| "META_CSOUNDPATCH";
	CEffectData			= ( _short ) && "_ed"			|| "META_CEFFECTDATA";
	CMoveData			= ( _short ) && "_md"			|| "META_CMOVEDATA";
	ITexture			= ( _short ) && "_texture"		|| "META_ITEXTURE";
	File				= ( _short ) && "_file"			|| "META_FILE";
	CTakeDamageInfo		= ( _short ) && "_damageinfo"	|| "META_CTAKEDAMAGEINFO";
	VMatrix				= ( _short ) && "_matrix"		|| "META_VMATRIX";
	Angle				= ( _short ) && "_ang"			|| "META_ANGLE";
	PhysObj				= ( _short ) && "_phys"			|| "META_PHYSOBJ";

	// SERVER
	CLuaLocomotion		= ( _short ) && "_locomotion"	|| "META_CLUALOCOMOTION";
	NextBot				= ( _short ) && "_nextbot"		|| "META_NEXTBOT";
	//Database			= ( _short ) && "META_DATABASE"	|| "META_DATABASE";
	PathFollower		= ( _short ) && "_pathfollower"	|| "META_PATHFOLLOWER";
	CRecipientFilter	= ( _short ) && "_rf"			|| "META_CRECIPIENTFILTER";
	CNavArea			= ( _short ) && "_nav"			|| "META_CNAVAREA";

	// CLIENT
	Panel				= ( _short ) && "_panel"		|| "META_PANEL";
	CLuaEmitter			= ( _short ) && "_emitter"		|| "META_EMITTER";
	CLuaParticle		= ( _short ) && "_particle"		|| "META_PARTICLE";
	IGModAudioChannel	= ( _short ) && "_audio"		|| "META_GMODAUDIOCHANNEL";

	_LOADLIB			= ( _short ) && "_load"			|| "META_LOADLIB";
	//_PRELOAD			= ( _short ) && "META_PRELOAD"	|| "META_PRELOAD";
	//_LOADED			= ( _short ) && "META_LOADED"	|| "META_LOADED";

	[ "table" ]			= ( _short ) && "_tab"			|| "META_TABLE";
	[ "bool" ]			= ( _short ) && "_b"			|| "META_BOOLEAN";
	[ "function" ]		= ( _short ) && "_func"			|| "META_FUNCTION";
	[ "number" ]		= ( _short ) && "_num"			|| "META_NUMBER";
	[ "string" ]		= ( _short ) && "_string"		|| "META_STRING";
};


//
// ENUMeration / CONSTs / etc...
//


//
// Configuration...
//
data.__config = {
	// Delay that un-linked data be re-requested by the client; may move this to the data class
	// How long should we keep values cached before re-requesting them ( on private flags the client isn't linked to and inquires about )
	data_refresh_rate				= 3;
};


//
// Important includes...
//
require_once( "shared/sh_networking.lua" );


//
// Helper for data:BuildAccessors/data:AccessorFunc to process forced values; logic was the same so instead of repeating...
//
local function BuildForcedXTable( _values, _func )
	local _tab = { };
	if ( _values ) then
		if ( istable( _values ) ) then
			for k, v in pairs( _values ) do
				_tab[ ( _func && _func( v ) || v ) ] = true;
			end
		else
			_tab[ ( _func && _func( _values ) || _values ) ] = true;
		end
	end

	return _tab;
end


//
// Simple helper.. Checks to see if we're forcing values or types, and if so checks to see if value is allowed...
//
local function ForcedXChecker( _forced, _allowed, _value )
	if ( _forced ) then
		if ( !_allowed[ _value ] ) then
			return false;
		end
	end

	return true;
end


//
// Adds Getters / Setters / Adders for flag...
//
// The _flag will be set to lowercase and used as the flag, so use UpperCamel or your naming technique
// for the function naming. If you want the flag to have a different name perhaps with underscores,
// simply set name = "Invisible" in the options table, then that name overwrites the function names
// and flag stays as flag ( still set to lower )...
//
// Examples:
//
// data:BuildAccessors( _flag, _default, _private, _category, _table, _force_types, _force_values, _options )
// data:BuildAccessors( "Invisible", false, false, FLAG_PLAYER, META_PLAYER, TYPE_BOOL, nil, { get_prefix = ""; } )
// 	Creates: _p:Invisible( ); _p:SetInvisible( true/false ); _p:CanSetInvisible( _value ); where CanSet
// simple checks to see if the input value is allowed. It is automatically added inside of the Setter...
// The default value IS NOT CHECKED before creating the functions...
//
// data:BuildAccessors( "player_admin", false, false, FLAG_PLAYER, META_PLAYER, TYPE_BOOL, nil, { name = "Admin"; get_prefix = "Is"; } )
// Creates: _p:IsAdmin( ); _p:SetAdmin( true/false ); _p:CanSetAdmin( _value );
//
// You can force the type of data and you can also force the allowed values. This doesn't apply to tables
// because if you're allowing a table of data, there may be a good reason, just use the parent data-type,
// table, to declare it; I may add support but it'd mean looping through and this system is meant to be fast...
//
// NOTE: When forcing data on NON-SIMPLE-DATA-TYPES I was forced to tostring the data in order to compare
// because Vector( 0, 0, 0 ) is not the same as Vector( 0, 0, 0 ) ( 2 different references and I haven't
// created the __eq comparitor for all meta-table objects yet ). So, they'll be turned to simple data-type
// string for comparison. So for some you'll have no issue, others you may...
//
// Examples of forcing values...:									for my vehicle system	( 0 = off, 1 = left, 2 = right, 3 = hazards )
// data:BuildAccessors( "signals", false, false, FLAG_VEHICLE, META_VEHICLE, TYPE_NUMBER, { 0, 1, 2, 3 }, { name = "TurnSignals"; get_prefix = ""; } );
// Creates: _v:TurnSignals( ); _v:SetTurnSignals( 0-3 ); _v:CanSetTurnSignals( x ); Which operate "signals" flag in category "vehicle"
//
function data:BuildAccessors( _flag, _default, _private, _category, _table, _force_types, _force_values, _options )
	local _debug_output = "Adding the following Accessors:\n";
	local _debug_table = data.__debug_tables[ _table.MetaName ] && data.__debug_tables[ _table.MetaName ] || ( _table.MetaName && _table.MetaName || "Unknown" );

	// Allowed Types and Allowed Values; will be at minimum empty table or direct-access one...
	local _allowed_types = BuildForcedXTable( _force_types, nil );
	local _allowed_values = BuildForcedXTable( _force_values, tostring );

	// Function names. Easier to pre-set because I need to use: _table[ _name ] = function( ) ... end and _table[ _name ]( self, ... );
	local _funcname		= ( _options.name || _flag );
	local _getprefix	= ( _options.get_prefix || "Get" );
	local _hasprefix	= ( _options.has_prefix || "Has" );

	if ( _getprefix == _hasprefix ) then
		error( "HasPrefix can not be the same as GetPrefix otherwise a collision of functions will occur and they perform different actions!" );
	end

	// Controller Names
	local _canset		= "CanSet" .. _funcname;
	local _scalevalue	= "Scale" .. _funcname;
	local _onchanged	= "OnChanged" .. _funcname;
	local _onfailed		= "OnFailed" .. _funcname;

	// Setter Names - these can alter the value
	local _setter		= "Set" .. _funcname;
	local _setmax		= "SetMax" .. _funcname;
	local _setmin		= "SetMin" .. _funcname;
	local _adder		= "Add" .. _funcname;
	local _toggle		= "Toggle" .. _funcname;

	// Getter Names - these can only give us data
	local _has			= _hasprefix .. _funcname;
	local _getter		= _getprefix .. _funcname;
	local _getmax		= _getprefix .. "Max" .. _funcname;
	local _getmin		= _getprefix .. "Min" .. _funcname;

	// Other
	-- local _clamper	= _options.adder_clamp && math.Clamp || function ( _value, _min, _max ) return _value; end

	//
	// CALLBACKS...
	//

	//
	// CanSet* - Hook which can prevent the number from changing
	//
	_debug_output = _debug_output .. _debug_table .. ":" .. _canset .. "( _value, _delta );";
	_table[ _canset ] = function ( self, _value, _delta )

		-- Override Function --

		// If _options.CanSetCallback then we use that one in the default function
		local _canset = true;
		if ( isfunction( _options.CanSetCallback ) ) then
			_canset = _options.CanSetCallback( self, _value, _delta );
		end

		// Default return true ( nothing works too.. any non-false number );
		return ( _canset != false ) && true;
	end

	//
	// Scale* - Returns a number to multiply against the _delta variable ( ie if sprinting return 2 to double the value added )
	//
	if ( _options.adder ) then
		_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _scalevalue .. "( _value, _delta );";
		_table[ _scalevalue ] = function ( self, _value, _delta )

			-- Override Function --

			// If _options.ScaleCallback then we use that one in the default function...
			local _scaled = 1;
			if ( isfunction( _options.ScaleCallback ) ) then
				_scaled = _options.ScaleCallback( self, _value, _delta );
			end

			// Default return current delta... Here you can multiply the delta value to change it to whatever you want...
			return _scaled;
		end
	end


	//
	// OnChanged* - Hook which is called if the value has successfully changed
	//
	_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _onchanged .. "( _value, _delta );";
	_table[ _onchanged ] = function ( self, _value, _delta )

		-- Override Function --

		if ( isfunction( _options.OnChangedCallback ) ) then
			_options.OnChangedCallback( self, _value, _delta );
		end
	end


	//
	// OnFailed* - Hook which is called if the value has NOT been changed
	//
	_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _onfailed .. "( _value, _delta );";
	_table[ _onfailed ] = function ( self, _value, _delta )

		-- Override Function --
		if ( isfunction( _options.OnFailedCallback ) ) then
			_options.OnFailedCallback( self, _value, _delta );
		end
	end


	//
	// Set* - Sets the value
	//
	_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _setter .. "( _value );";
	_table[ _setter ] = function ( self, _value )
		// Verifies data-types and values if forced...
		local _bTypeAllowed = ForcedXChecker( _force_types, _allowed_types, TypeID( _value ) );
		local _bValueAllowed = ForcedXChecker( _force_values, _allowed_values, tostring( _value ) );

		// Uses a callback to ensure the data can be set..
		local _delta = _value - self[ _getter ]( self );
		local _bCanSet = hook.Call( _canset, self, self, self[ _getter ]( self ), _delta );

		// If the callback doesn't stop us, and the data types or values don't stop us... allow setting...
		if ( ( _bCanSet != false ) &&_bTypeAllowed && _bValueAllowed ) then
			// If we're in adder and the value is too low or too high, set them to the limit.
			local _bTooLow = ( _options.min && _value < self[ _getmin ]( self ) );
			local _bTooHigh = ( _options.max && _value > self[ _getmax ]( self ) )
			if ( _options.adder && ( _bTooLow || _bTooHigh ) ) then
				if ( _bTooLow ) then
					self:SetFlag( string.lower( _flag ), self[ _getmin ]( self ), _private, _category );
				elseif ( _bTooHigh ) then
					self:SetFlag( string.lower( _flag ), self[ _getmax ]( self ), _private, _category );
				else
					// This should never happen, this shouldn't be here...
					error( "Error with option adder min and max..." );
				end
			else
				// Otherwise we're NOT in adder mode
				// OR we aren't exceeding max or falling below minimum so it's safe to set...
				self:SetFlag( string.lower( _flag ), _value, _private, _category );
			end

			// Called after successfully changing the value...
			hook.Call( _onchanged, self, self, self[ _getter ]( self ), _delta );

			return true;
		end

		// Called after failing to change the value...
		hook.Call( _onfailed, self, self, self[ _getter ]( self ), _delta );

		return false;
	end


	//
	// "Get"* - Returns the value stored in this flag...
	//
	_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _getter .. "( );";
	_table[ _getter ] = function ( self )
		return self:GetFlag( string.lower( _flag ), _default, _private, _category );
	end


	//
	// Toggle* - Only used on boolean functions
	//
	if ( _options.toggle && ( _force_types == TYPE_BOOL || _allowed_types[ TYPE_BOOL ] || ( _allowed_values[ true ] && _allowed_values[ false ] ) ) ) then
		_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _toggle .. "( );";
		_table[ _toggle ] = function ( self )
			_setter( self, !self[ _getter ]( self ) );
		end
	end


	// Adder Functions
	if ( _options.adder && ( _force_types == TYPE_NUMBER || _allowed_types[ TYPE_NUMBER ] ) ) then
		// If we have max set allow max functions
		if ( _options.max ) then
			//
			// SetMax* - Updates the maximum value allowed
			//
			_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _setmax .. "( _value );";
			_table[ _setmax ] = function ( self, _value )
				self:SetFlag( string.lower( _flag ) .. "_max", _value, _private, _category );
			end


			//
			// "Get"Max* Which returns the maximum value allowed...
			//
			_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _getmax .. "( _value );";
			_table[ _getmax ] = function ( self )
				return self:GetFlag( string.lower( _flag ) .. "_max", _options.max, _private, _category );
			end
		end

		// If we have min set allow min functions
		if ( _options.min ) then
			//
			// SetMin* - Updates the minimum value allowed
			//
			_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _setmin .. "( _value );";
			_table[ _setmin ] = function ( self, _value )
				self:SetFlag( string.lower( _flag ) .. "_min", _value, _private, _category );
			end


			//
			// "Get"Min* Which returns the minimum value allowed...
			//
			_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _getmin .. "( );";
			_table[ _getmin ] = function ( self )
				return self:GetFlag( string.lower( _flag ) .. "_min", _options.min, _private, _category );
			end
		end


		//
		// Add* which adds a value to another ( The Setter controls clamping if necessary )...
		//
		_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _adder .. "( _delta );";
		_table[ _adder ] = function ( self, _delta )
			// We call Scale* inside the adder which lets us multiply the delta
			local _scale = hook.Call( "Scale" .. _funcname, self, self, self[ _getter ]( self ), _delta );
			if ( !isnumber( _scale ) ) then
				_scale = 1;
			end

			_table[ _setter ]( self, self[ _getter ]( self ) + ( _delta * _scale ) );
		end
	end


	//
	// Has* - Returns true if we don't input value and min exists and there is more than min
	//			Returns true if value is input and we have more than it
	//			Returns true if value is non-false/nil
	//
	_debug_output = _debug_output .. "\n" .. _debug_table .. ":" .. _has .. "( [ _value ] );";
	_table[ _has ] = function ( self, _value )
		if ( !_value && _options.min ) then
			return self[ _getter ]( self ) > self[ _getmin ]( self );
		elseif ( _value ) then
			return self[ _getter ]( self ) >= _value;
		else
			return self[ _getter ]( self ) && true || false;
		end
	end

	// Debug Output
	data.__accessors_log = data.__accessors_log .. _debug_output .. "\n\n";
	-- fileio:Write( "", "accessors_log", data.__accessors_log );
	debug.print( "Data:BuildAccessors", _debug_output .. "\n" );

	// Return a table of references to the functions for the purpose of adding aliases...
	return {
		// What the name of the function is..
		func_name		= _funcname;

		// Which prefix we're using for the getters
		get_prefix		= _getprefix;

		// Which prefix we're using for the Has
		has_prefix		= _hasprefix;

		// Internal.. Checks to make sure the data-type / value being applied is allowed.
		CanSet			= _table[ _canset ];

		// Gets the current value
		Get				= _table[ _getter ];

		// Sets a value
		Set				= _table[ _setter ];

		// Adds a value ( can use - )
		Add				= _table[ _adder ];

		// Checks to see user HAS something ( number = > 0, bool = true, input value = number >= x )
		Has				= _table[ _has ];

		// Part of min flag; ensures value never goes below
		SetMin			= _table[ _setmin ];
		GetMin			= _table[ _getmin ];

		// Part of max flag; ensures value never goes above
		SetMax			= _table[ _setmax ];
		GetMax			= _table[ _getmax ];

		// Type Bool = toggle false / true..
		Toggle			= _table[ _toggle ];

		// Callbacks
		-- AddCallback		= _table[ _adder ];
		CanAddCallback	= _table[ _toggle ];
		ScaleCallback	= _table[ _toggle ];
	};
end


//
// For those that want to use AccessorFunc naming technique...
//
// AccessorFunc( table tab, any key, string name, number iForce )
// AccessorFunc( META_PLAYER, "invisible", "Invisible", TYPE_BOOL )
//
// I decided to not use the AccessorFunc method because it didn't allow for all the options
// and didn't go with the normal format of the flag system( flag, default/value, private, category )
// however, data:AccessorFunc is setup in a similar method. It calls the function below with reorganized
// naming...
//
// These are identical:
// data:BuildAccessors( "player_admin", false, false, FLAG_PLAYER, META_PLAYER, TYPE_BOOL, nil, { name = "Admin"; get_prefix = "Is"; } )
// data:AccessorFunc( META_PLAYER, "player_admin", "Admin", TYPE_BOOL, nil, false, false, FLAG_PLAYER, { get_prefix = "Is"; } )
// Creates: _p:IsAdmin( ); _p:SetAdmin( true/false ); _p:CanSetAdmin( x );
//
function data:AccessorFunc( _table, _flag, _name, _force_types, _force_values, _default, _private, _category, _options )
	local _options = _options || { };
	_options.name = _name;

	data:BuildAccessors( _flag, _default, _private, _category, _table, _force_types, _force_values, _options )
end


//
// Grab the private or public data list
//
function data:GetFlags( _private )
	if ( _private ) then
		return self.__flags.private;
	end

	return self.__flags.public;
end


//
// Determines whether or not a data table exists ( useful for quick checks )
//
function data:TableExists( _id, _private, _category )
	if ( !_id ) then return false; end
	if ( isentity( _id ) ) then _id = _id:GetID( ); end

	local _data = self:GetFlags( _private );

	if ( _data[ _id ] ) then
		if ( !_category ) then
			if ( _data[ _id ].default ) then
				return true;
			end
		else
			if ( _data[ _id ].lists && _data[ _id ].lists[ _category ] ) then
				return true;
			end
		end
	end

	return false;
end


//
// Initialize tables needed...
//
function data:InitTables( _id, _private, _category )
	if ( !_id ) then return false; end
	if ( isentity( _id ) ) then _id = _id:GetID( ); end

	local _data = self:GetFlags( _private );

	if ( !_data[ _id ] ) then
		_data[ _id ] = { };
	end

	if ( _category ) then
		if ( !_data[ _id ].lists ) then
			_data[ _id ].lists = { };
		end

		if ( !_data[ _id ].lists[ _category ] ) then
			_data[ _id ].lists[ _category ] = { };
		end
	else
		if ( !_data[ _id ].default ) then
			_data[ _id ].default = { };
		end
	end
end


//
// Returns the list of players an entity is linked to
//
function data:GetLinks( _ent )
	local _id, _ent	= self:GetEntityID( _ent );
	local _links = self.__flags.links[ _id ] || { };

	return _links;
end


//
// Updates the public data flags table with an inbound table ( for syncing )
//
function data:SetFlags( _flags )
	debug.print( "Data:InitializePublic", "SetFlags:", _flags );
	self.__flags.public = _flags;
end


//
// Updates the default private-list data flags table with an inbound table ( for syncing specific entities / pdata )
//
function data:SetPData( _id, _tab )
	if ( !self.__flags.private[ _id ] ) then
		self.__flags.private[ _id ] = {
			default = { };
			lists = { };
		};
	end

	debug.print( "Data:InitializePrivate", "SetPData EntityID, Data, ID:", _id, "Table:", _tab );
	self.__flags.private[ _id ] = _tab;
end


//
// Returns the entity id
//
function data:GetEntityID( _ent )
	if ( isstring( _ent ) || isnumber( _ent ) ) then
		local _id = tonumber( _ent ) || -1;
		return _id, Entity( _id );
	else
		if ( IsValid( _ent ) ) then
			local _id = tonumber( _ent:GetID( ) ) || -1;
			return _id, _ent;
		else
			debug.print( "Data:GetEntityIDError", type( _ent ) .. " " .. _ent .. " ); isn't valid..." );

			return 0, game.GetWorld( );
		end
	end
end


//
// Returns the entity flag value ( This is called by META_ENTITY:GetFlag( _flag, _default, _private ) )
//
function data:GetFlag( _ent, _flag, _default, _private, _category, _request )
	local _id, _ent	= self:GetEntityID( _ent );
	local _flags 	= self:GetFlags( _private );

	if ( _id >= 0 ) then
		// If CLIENT is running this code,
		// and the variable is a private var ( which won't sync for non-LocalPlayer if different entity unless linked )
		// And making sure _ent isn't LocalPlayer ( because LP data will sync automatically; non-lp entities won't unless linked )
		if ( CLIENT && _private && _ent != LocalPlayer( ) ) then
			local _bLinked = self:IsLinked( LocalPlayer( ), _ent );

			// If the client isn't linked to this particular entity ( but the client is requesting the data ),
			// send a request, but only if the client wants it ( By default it isn't on to save networking speed,
			// and because it is more rare to request a variable than it is to want to set private data from the
			// client onto other players / objects without it being erased by the request ); this way we don't need
			// to store ALL information we know of the world / ents on the world in public realm or on other ents
			// in public realm to prevent requests... We can store our own private data on other ents such as name
			// if they tell us, etc...
			if ( !_bLinked && _request ) then
				// Get the current value of the flag to send for comparison
				local _data = _ent:GetFlag( _flag, _default, _private, _category, false );

				// Requests data from the server based on an interval if we don't have a linked data connection.
				networking:RequestFlag( ( isnumber( _request ) && _request || data.__config.data_refresh_rate ), _ent, _flag, _data, _private, _category, _default );

				debug.print( "Data:RequestPrivate", "GetFlag > RequestFlag, Entity:", _ent, "Flag:", _flag, "Default:", _default, "Private:", _private );
			end
		end

		// If our database is setup, and the GetID exists, and either the flag exists / is set, or it is false ( because values can switch between true/false and they still need to be updated )
		if ( _flags && _flags[ _id ] ) then
			local _localflags = _flags[ _id ];
			if ( _category || _category == false ) then
				if ( !_localflags.lists ) then _localflags.lists = { }; end
				if ( !_localflags.lists[ _category ] ) then _localflags.lists[ _category ] = { }; end

				_localflags = _localflags.lists[ _category ];
			else
				if ( !_localflags.default ) then _localflags.default = { }; end
				_localflags = _localflags.default;
			end
			if ( _localflags[ _flag ] || _localflags[ _flag ] == false ) then
				debug.print( "Data:Get", "GetFlag > Entity:", _ent, "Flag:", _flag, "Default:", _default, "Private:", _private, "Category:", _category );
				return _localflags[ _flag ];
			end
		end
	end

	// Return default if all else fails...
	debug.print( "Data:GetDefault", "GetFlag > Default, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
	return _default;
end


//
// Sets the entity flag value ( This is called by META_ENTITY:SetFlag( _flag, _value, _private ) )
//
function data:SetFlag( _ent, _flag, _value, _private, _category )
	local _id, _ent	= self:GetEntityID( _ent );

	local _flags = self:GetFlags( _private );
	if ( _id >= 0 ) then
		if ( !_flags[ _id ] ) then
			_flags[ _id ] = { };
		end

		// Added to allow private-flag lists ( for addons to avoid collisions )
		// Now includes public flags by switching to an optional category system
		local _localflags = _flags[ _id ];
		if ( _category || _category == false ) then
			if ( !_localflags.lists ) then _localflags.lists = { }; end
			if ( !_localflags.lists[ _category ] ) then _localflags.lists[ _category ] = { }; end

			_localflags = _localflags.lists[ _category ];
		else
			if ( !_localflags.default ) then _localflags.default = { }; end

			_localflags = _localflags.default;
		end

		// If the data hasn't changed, don't do anything. Tables are always updated until I change that behavior
		if ( _localflags[ _flag ] == _value && !istable( _localflags[ _flag ] ) ) then
			debug.print( "Data:SetFlagNoChangeError", "No Update, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
			return;
		end

		// Update the value...
		debug.print( "Data:SetFlag", "Updated Flag, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
		_localflags[ _flag ] = _value;

		// Server code
		if ( SERVER ) then
			// If this is a private flag ( Only known to server, or server and one client )
			if ( _private ) then
				// If it's to be known with a client, let the client know, otherwise it's server only
				if ( _ent:IsPlayer( ) ) then
					debug.print( "Data:SetPrivateFlag", "Updated Flag, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
					networking:SendToClient( "EntitySetFlag", _ent, _id, _flag, _value, _private, _category );
				else
					local _links = self:GetLinks( _id );
					if ( table.Count( _links ) > 0 ) then
						debug.print( "Data:SetPrivateLinkedFlag", "Updated Flag, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
						networking:SendToClient( "EntitySetFlag", _linkedlist, _id, _flag, _value, _private, _category );
					end
				end
			else
				// Globally known flag, Tell the world
				debug.print( "Data:SetPublicFlag", "Updated Flag, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
				networking:Broadcast( "EntitySetFlag", _id, _flag, _value, false, _category );
			end
		end
	end
end


//
//
//
function data:OnRemoveData( _id, v, _type, _typeid )
	if ( _typeid == TYPE_SOUND ) then
		if ( v:IsPlaying( ) ) then
			-- print( "Sound playing: ", v );
			v:Stop( );
		end
	end
end


//
// Simple helper-function to recursively chew through data and clear it from memory...
//
local function ResetTableFlags( _id, _tab )
	if ( istable( _tab ) ) then
		for k, v in pairs( _tab ) do
			if ( istable( v ) ) then
				ResetTableFlags( _id, v );
			else
				data:OnRemoveData( _id, v, type( v ), TypeID( v ) );
				hook.Call( "OnRemoveData", GAMEMODE, v, type( v ), TypeID( v ) );
				v = nil;
			end
		end
	end

	_tab = nil;
end


//
// Resets all flags and links associated with an entity
//
function data:ResetFlags( _id ) // , _private
	//
	local _flags = self:GetFlags( false );
	local _entflags = _flags[ _id ];
	local _pflags = self:GetFlags( true );
	local _entpflags = _pflags[ _id ];

	if ( SERVER ) then
		local _links = self:GetLinks( _id );
		if ( _links ) then
			_links = nil;
		end
	end

	if ( _entflags || _entpflags ) then
		ResetTableFlags( _id, _entflags );
		ResetTableFlags( _id, _entpflags );
		_flags[ _id ] = nil;
		_pflags[ _id ] = nil;

		if ( SERVER ) then
			debug.print( "Data:ResetFlags", "Networking EntityResetFlags, ID:", _id );
			networking:Broadcast( "EntityResetFlags", _id, _private );
		end

		debug.print( "Data:ResetFlags", "Reset Public / Private Flags, ID:", _id );
		return true;
	end

	debug.print( "Data:ResetFlagsError", "No Flags to Reset, ID:", _id );
	return false;
end


//
// Checks to see if _ent is linked to _p
//
function data:IsLinked( _p, _ent )
	local _pid, _p = self:GetEntityID( _p );
	local _id, _ent = self:GetEntityID( _ent );

	if ( !self.__flags.links[ _id ] ) then self.__flags.links[ _id ] = { }; end
	local _links = self.__flags.links[ self:GetEntityID( _ent ) ];
	local _linked = _links[ self:GetEntityID( _p ) ];

	return _linked == _p;
end


//
// INTERNAL: Networks the link update to client / server...
//
function data:__UpdateLink( _p, _ent, _active )
	debug.print( "Data:UpdateLink", "Player:", _p, "Entity:", _ent, "Active:", _active );

	if ( CLIENT ) then
		networking:SendToServer( "UpdateDataLink", _ent, _active );
	else
		networking:SendToClient( "UpdateDataLink", _p, _ent, _active );
	end
end


//
// INTERNAL: Handles linking / unlinking of data
//
function data:__LinkData( _p, _ent, _active )
	// Grab ids
	local _pid, _p = self:GetEntityID( _p );
	local _id, _ent = self:GetEntityID( _ent );

	// Ensure initialized
	if ( !self.__flags.links[ _id ] ) then self.__flags.links[ _id ] = { }; end

	// Link the data...
	if ( !self.__flags.links[ _id ][ _pid ] && _active ) then
		self.__flags.links[ _id ][ _pid ] = _p;
		data:__UpdateLink( _p, _ent, _active );
	elseif ( self.__flags.links[ _id ][ _pid ] && !_active ) then
		self.__flags.links[ _id ][ _pid ] = nil;
		data:__UpdateLink( _p, _ent, _active );
	end
end


//
// Add a link so that private flags for _ent will also be sent to _p
//
function data:LinkData( _p, _ent )
	self:__LinkData( _p, _ent, true );

	if ( SERVER ) then
		debug.print( "Data:LinkData", "Player:", _p, "Entity:", _ent );
		local _id, _ent = self:GetEntityID( _ent );
		networking:SendToClient( "SyncPData", _p, _id, self.__flags.private[ _id ] );
	end
end


//
// Terminates private data link
//
function data:UnlinkData( _p, _ent )
	self:__LinkData( _p, _ent, false );

	if ( CLIENT ) then
		debug.print( "Data:UnLinkData", "Player:", _p, "Entity:", _ent );
		local _id, _ent = self:GetEntityID( _ent );
		self.__flags.private[ _id ] = nil;
	end
end


//
// Networking
//
if ( CLIENT ) then
	//
	// Updates a flag
	//
	networking:AddReceiver( "EntitySetFlag", function( _lp, _ent, _flag, _value, _private, _category )
		debug.print( "Data:RcvEntitySetFlag", "Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private, "Category:", _category );
		data:SetFlag( _ent, _flag, _value, _private, _category );
	end );


	//
	// Processes resetting of all flags...
	//
	networking:AddReceiver( "EntityResetFlags", function( _lp, _id )
		debug.print( "Data:RcvEntityResetFlags", "ID:", _id );
		local _ent = Entity( _id );
		data:ResetFlags( _id );
	end );


	//
	// Network Receiver which grabs all private data and sets it on the client
	//
	networking:AddReceiver( "SyncPData", function( _lp, _id, _tab )
		debug.print( "Data:RcvSyncPData", "ID:", _id );
		data:SetPData( _id, _tab )
	end );


	//
	// Network Receiver which grabs all public data and sets it on the client
	//
	networking:AddReceiver( "SyncData", function( _lp, _tab )
		debug.print( "Data:RcvSyncData", "ID:", _id );
		return data:SetFlags( _tab );
	end );
else
	//
	// Remove Data when the entity is removed...
	//
	hook.Add( "EntityRemoved", "ResetEntityFlags", function( _ent )
		// No point in continuing if the entity isn't valid...
		if ( !IsValid( _ent ) ) then return; end

		// Entity ID / EntIndex
		local _id = _ent:GetID( );

		// Remove both private and normal flags on removal after a second to allow any func to save...
		timer.Simple( 1, function( )
			data:ResetFlags( _id );
		end );
	end );


	//
	// Link Private Vehicle Data to the Driver
	//
	hook.Add( "PlayerEnteredVehicle", "LinkDriverVehicleData", function( _p, _v, _role )
		// Link private data...
		data:LinkData( _p, _v );

		// Share the hook with the client
		networking:CallHook( "PlayerEnteredVehicle", _p, "GAMEMODE", _p, _v, _role );

		// Prevent binds for x time ( so use key doesn't trigger anything until x time after getting into vehicle )
		_p:SetFlag( "vehicle_entry", CurTime( ) );
	end );


	//
	// Unlink Private Vehicle Data to the Driver
	//
	hook.Add( "PlayerLeaveVehicle", "UnlinkDriverVehicleData", function( _p, _v )
		// Unlink private data...
		data:UnlinkData( _p, _v );

		// Share the hook with the client
		networking:CallHook( "PlayerLeaveVehicle", _p, "GAMEMODE", _p, _v );
	end );
end


//
// Processes the Linking and Unlinking of data
//
networking:AddReceiver( "UpdateDataLink", function( _p, _ent, _active )
	debug.print( "Data:RcvUpdateDataLink", "Player:", _p, "Entity:", _ent, "Active:", _active );
	if ( !_active ) then
		data:UnlinkData( _p, _ent );
	else
		data:LinkData( _p, _ent );
	end
end );