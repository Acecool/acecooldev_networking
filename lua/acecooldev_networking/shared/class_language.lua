//
// Language Class - Josh 'Acecool' Moser
// Places special functions in a special class. This is the primary container for data management, etc.
//


//
// Color CONSTs; uncomment if you're not using the color-defs file.
//
--local COLOR_CYAN 		= Color(	0, 255, 255, 255 );
--local COLOR_GREEN 	= Color(	0, 255,	0, 255 );
--local COLOR_YELLOW 	= Color( 255, 255,	0, 255 );
--local COLOR_RED 		= Color( 255,	0,	0, 255 );


//
// Globals
//
LANGUAGE_DEFAULT = "en";


//
// Init..
//
language = language || { };
language.languages = { };
language.__index = language;
language.__type = "Language";
language.__current = nil;
language.__default = LANGUAGE_DEFAULT;


//
// Use this to change the selected language
//
function language.Set( _lang )
	if ( !_lang ) then return false; end
	if ( !language.languages[ _lang ] ) then MsgC( COLOR_RED, "#lang_doesnt_exist" ); end

	language.__current = _lang;
	return _lang;
end


//
// Alias
//
function language.SetLanguage( _lang )
	return language.Set( _lang );
end


//
// Alias
//
function language.Get( )
	return ( CLIENT && LocalPlayer( ).GetLanguage ) && LocalPlayer( ):GetLanguage( ) || language.__current || language.__default;
end


//
// Alias
//
function language.GetLanguage( )
	return language.Get( );
end


//
// Removes # from phrase and turns spaces into underscores. When grabbing entry, spaces or underscores can be used...
//
function language.GetSafePhrase( _phrase )
	local _phrase = string.lower( _phrase );
	_phrase = string.gsub( _phrase, " ", "_" );
	_phrase = string.gsub( _phrase, "#", "" );
	return _phrase;
end


//
// Converts dictionary string to all-lowercase
//
function language.GetSafeDictionary( _lang )
	// Rewrite the var to be all lowercase, or nil so the || takes care of it..
	local _lang = ( _lang ) && string.lower( _lang ) || nil;
	_lang = _lang || language.__current || language.__default;

	return _lang;
end


//
// Ensures the dictionary being requested is initialized
//
function language.InitDictionary( _lang )
	if ( !language.__dictionary ) then language.__dictionary = { }; end

	if ( !language.__dictionary[ _lang || language.__default ] ) then
		language.__dictionary[ _lang || language.__default ] = { };
	end
end


//
// Adds a language phrase to the defined ( or, if blank then the default ) dictionary.
//
function language.Add( _phrase, _text, _lang )
	// Ensures the dictionary and key/phrase are "safe"
	local _lang = language.GetSafeDictionary( _lang );
	local _phrase = language.GetSafePhrase( _phrase );

	// Make sure the requested dictionary is initialized
	language.InitDictionary( _lang );

	// If debug is enabled, then print out how words are added
	if ( DEBUG ) then
		MsgC( COLOR_GREEN, "[" .. GAMEMODE.Name .. "]-LANGUAGE : Add [", COLOR_CYAN, _lang, COLOR_GREEN, "][", COLOR_YELLOW, _phrase, COLOR_GREEN, "] = \"", COLOR_RED, _text, COLOR_GREEN, "\";\n" );
	end

	language.__dictionary[ _lang ][ _phrase ] = _text;
end


//
// Backwards Compatability System
//
function language.GetPhrase( _phrase, ... )
	// Converts input phrase and dictionary name to "safe"
	local _phrase = language.GetSafePhrase( _phrase );
	local _lang = language.GetSafeDictionary( language.Get( ) );

	// Ensure dictionary is initalized
	language.InitDictionary( _lang );

	// Grabs phrase data, if it doesn't exist it prints "!L[ <key> ]"
	local _output = language.__dictionary[ _lang ][ _phrase ] || "!L[ " .. _phrase .. " ]";

	// If this string is to be formatted ( extra arguments ) then process it, otherwise just return it
	if ( table.Count( { ... } ) > 0 ) then
		return string.format( _output, ... );
	else
		return _output;
	end
end


//
// Allow language( "garry"[, "en"] ); instead of using language.GetPhrase( "garry"[, "en"] );
// Meta-tables must be active for this to work..
//
-- language.__call = function( self, _phrase, _lang, ... )
	-- return language.GetPhrase( _phrase, _lang, ... );
-- end;


//
// Example of language system...
//
function language:GenerateExample( )
	language.Add( "garry", "Garry Newman", "en" );
	print( "#garry2", "Red means that the language phrase wasn't found; it still prints out the attempted phrase for debugging purposes." );
	print( "#Garry", "Case doesn't matter, Cyan means that it is a functional language phrase using the hash call-operator!" );
	print( language.GetPhrase( "garry" ), "White means that the language class was directly called and then output to text instead of using the #modifier.\n" );
	-- print( language.GetPhrase( "garry" ) );

	language.Add( "Acecool", "Josh 'Acecool' Moser", "en" );
	language.Add( "Josh 'Acecool' Moser", "Josh 'Acecool' Moser" ); -- Language added to default dictionary doesn't need the argument..
	print( "#acecool2", "Red means that the language phrase wasn't found; it still prints out the attempted phrase for debugging purposes." );
	print( "#acecOOl", "Case doesn't matter, Cyan means that it is a functional language phrase using the hash call-operator!" );
	print( language.GetPhrase( "ACECOOL" ), "White means that the language class was directly called and then output to text instead of using the #modifier.\n" );
	-- print( language.GetPhrase( "acecool" ) );
	-- print( language.GetPhrase( "josh 'acecool' moser" ) );
end
concommand.Add( "dev_language_example", language.GenerateExample );