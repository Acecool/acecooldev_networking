//
//
//
require_once( "class_language", REALM_SHARED, true );
local _lang = "en";

language.Add( "datatype_example_unknown", "Examples Unavailable", _lang );
language.Add( "datatype_example_number", "Examples of <number>: -999999, -1234, -100, -10, 0, 10, 100, 1234, 999999, etc...", _lang );
language.Add( "datatype_example_boolean", "Examples of 'Boolean': true or false", _lang );
language.Add( "datatype_example_vector", "Examples of 'Vector': vector_origin, Vector( <number' x, <number> y, <number> z ) where <number> represents the data-type of each argument value", _lang );
language.Add( "datatype_example_angle", "Examples of 'Angle': angle_zero, Angle( <number> pitch, <number> yaw, <number> roll ) where <number> represents the data-type of each argument value", _lang );
language.Add( "assert_error", "failed", _lang );
language.Add( "assert_error", "Reported an AssertArgs( _map, ... ) Improper Argument-Map > Argument-Data Error! Argument data-type entered is '%s' ( %s ) containing the value '%s' when the data-type '%s' ( %s ) was expected instead; Verify you're passing the correct data-type and value to the function.", _lang );
