/*******************************************************************************************
 *
 * 						Acecool Company: AcecoolDev Base for Garry's Mod
 * 				Josh 'Acecool' Moser :: CEO & Sole Proprietor of Acecool Company
 * 			 Skeleton++ Game-Mode with many useful helper-functions and algorithms
 *
 * 			 Copyright � 2001-2014 Acecool Company in Name & Josh 'Acecool' Moser
 *	as Intellectual Property Rights Owner & Sole Proprietor of Acecool Company in name.
 *
 * 					 RELEASED Under the ACL ( Acecool Company License )
 *
 *******************************************************************************************/


//
// Developer Mode...
//
CreateConVar( "sv_devmode", "0", FCVAR_SERVER_CAN_EXECUTE + FCVAR_REPLICATED + FCVAR_NOTIFY + FCVAR_ARCHIVE );


//
//
//
function game.DeveloperMode( )
	return ( GetConVarNumber( "sv_devmode" ) == 1 );
end


//
// Enumerations
//


//
AUTOLOADER_LOADED_ADDON		= AUTOLOADER_LOADED_ADDON || false;
AUTOLOADER_LOADED_ADDON_TIME	= AUTOLOADER_LOADED_ADDON_TIME || 0;

// If the file shouldn't be loaded, this is assigned
REALM_NOLOAD				= -1;

// This is the default realm; unknown / waiting to be calculated
REALM_UNKNOWN				= 0;

// These enumerations represent how the file is included
REALM_CLIENT				= 1;
REALM_SERVER				= 2;
REALM_SHARED				= 3;


// This servers as a translation map for the above enums in the default language
REALMS = {
	[ REALM_NOLOAD ]		= "Skipped";
	[ REALM_UNKNOWN ]		= "Unknown";
	[ REALM_CLIENT ]		= "Client";
	[ REALM_SHARED ]		= "Shared";
	[ REALM_SERVER ]		= "Server";
};


//
// Init Auto-Loader but retain information we want to retain on auto-refresh
//
autoloader = autoloader || { };


//
// Allow Auto Loader Config to change during Auto-Refresh
//
autoloader.config = {
	// Should we use smart-refresh ( used for linux or windows servers with auto-refresh disabled so they can auto-refresh )?
	smart_refresh		= false;

	// Whether the files found deepest should be loaded first or not ( if false it'll load shallow first )
	deep_first_include	= true;

	// Should the autoloader output load info "AutoLoader Processed x Client, x Shared, x Server, and x Skipped addon files!"
	output_load_info	= true;

	// Should we output which files are included in exact order ( can be useful for debugging )...
	output_inclusions	= true;

	// Should the inclusions be saved into a file?
	file_inclusions		= true;
};


//
// Reset runtime configuration ( So files can be loaded, etc... )
//
autoloader.runtime = autoloader.runtime || {
	base_folder			= "";

	// Default Folder Realms ( name of folder on left = realm on right for files in folder and deeper )
	folder_realms = {
		// Base Realm Directories
		client		= REALM_CLIENT;
		server		= REALM_SERVER;
		shared		= REALM_SHARED;

		// Special Directories [ process later or not?? ]
		entities	= REALM_SHARED;
		weapons		= REALM_SHARED;

		// Load Later Directories
		games		= REALM_SHARED;
		maps		= REALM_SHARED;

		//
	};

	// Default "Process Later" Folders
	process_later = {
		// maps/ folders get processed later by checking maps/<current_map>/
		maps = function( _folder, _file )
			return _folder .. _file .. "/" .. string.lower( game.GetMap( ) ) .. "/", REALM_SHARED;
		end;

		// games/ folders get processed later by checking games/<current_game>/
		games = function( _folder, _file )
			return _folder .. _file .. "/" .. string.lower( game.GetGame( ) ) .. "/", REALM_SHARED;
		end;
	};
};


// Cache; store folder / file realms here along with file-times and other info...
autoloader.cache = autoloader.cache || {
	data = { };
};

// Used in Smart Auto-Refresh in case we need to send all updated files to the client...
autoloader.cache.files_updated = { };


//
// Resets the runtime
//
function autoloader:Reset( )
	self.runtime.current_process = {
		// Files Loaded
		files = { };

		// File Counter
		file_count = {
			[ REALM_NOLOAD ]	= 0;
			[ REALM_UNKNOWN ]	= 0;
			[ REALM_CLIENT ]	= 0;
			[ REALM_SERVER ]	= 0;
			[ REALM_SHARED ]	= 0;
		};
	};
end


//
// To make the autoloader work in other settings, allow us to simply set the base folder ( which is added to inclusions )
//
function autoloader:SetBaseFolder( _folder, _path )
	// Make sure the base ends with slash because it'll be prefix'd onto all includes...
	if ( !string.EndsWith( _folder, "/" ) ) then _folder = _folder .. "/"; end
	_folder = string.gsub( _folder, "//", "/" );

	self.runtime.base_folder = _folder;
	self:SetWorkingFolder( _folder );
	self:SetBasePath( _path );
end


//
// Returns the base folder we're working under ( typically "gamemodes/<gm_folder>/gamemode/" )
//
function autoloader:GetBaseFolder( )
	return self.runtime.base_folder || "";
end


//
// To make the autoloader work in other settings, allow us to simply set the base folder ( which is added to inclusions )
//
function autoloader:SetWorkingFolder( _folder )
	// Make sure the base ends with slash because it'll be prefix'd onto all includes...
	if ( !string.EndsWith( _folder, "/" ) ) then _folder = _folder .. "/"; end
	_folder = string.gsub( _folder, "//", "/" );

	self.runtime.working_folder = _folder;
end


//
// Returns the base folder we're working under ( typically "gamemodes/<gm_folder>/gamemode/" )
//
function autoloader:GetWorkingFolder( )
	return self.runtime.working_folder || "";
end


//
// Returns the base path we're working under ( typically "LUA" )
//
function autoloader:SetBasePath( _path )
	self.runtime.base_path = _path;
end


//
// Returns the base path we're working under ( typically "LUA" )
//
function autoloader:GetBasePath( )
	return self.runtime.base_path || "LUA";
end


//
//
//
function autoloader:UpdateRealmFileCount( _realm )
	-- if ( !self.runtime.current_process.file_count ) then self.runtime.current_process.file_count = { }; end
	-- if ( !self.runtime.current_process.file_count[ _realm ] ) then self.runtime.current_process.file_count[ _realm ] = 0; end
	if ( _realm && REALMS[ _realm ] ) then
		self.runtime.current_process.file_count[ _realm ] = self.runtime.current_process.file_count[ _realm ] + 1;
	end
end


//
// Ensure our cache table system exists...
//
function autoloader:InitFolders( _folder, _file )
	local _cache = self.cache.data;
	if ( _folder ) then
		if ( !_cache[ _folder ] ) then
			_cache[ _folder ] = {
				folder_data = {
					realm = REALM_UNKNOWN;
				};

				files = { };
			};
		end

		if ( _file ) then
			if ( !_cache[ _folder ].files[ _file ] ) then
				_cache[ _folder ].files[ _file ] = {
					time = -1;
					realm = REALM_UNKNOWN;
				};
				self:UpdateFileTime( _folder, _file );
			end
		end
	end
end


//
// Helper Function; Tells us the realm of a full-folder
//
function autoloader:GetFolderRealm( _folder )
	self:InitFolders( _folder );

	// Get realm based on folder...
	local _realm = self.cache.data[ _folder ].folder_data.realm;

	// If we have a set realm for the deepest section of the folder ( O( 1 ) ) then use it.
	if ( _realm != REALM_UNKNOWN ) then return _realm; end

	return false;
end


//
// Update folder realm only if it needs it ( we aren't changing realms in real time ... or? )
//
function autoloader:SetFolderRealm( _folder, _realm )
	-- self:InitFolders( _folder );
	self.cache.data[ _folder ].folder_data.realm = _realm || REALM_UNKNOWN;
end


//
// Helper Function; Returns the file realm either by using cache or calculating...
//
function autoloader:GetFileRealm( _folder, _file )
	-- self:InitFolders( _folder, _file );

	local _realm = REALM_UNKNOWN;
	local _files = self.cache.data[ _folder ].files;
	if ( _files ) then
		_realm = _files[ _file ] && _files[ _file ].realm || _realm;
	end

	// Then check for any overrides ( unless it is set )
	if ( _realm ) then
		return _realm
	end

	return false;
end


//
// Update folder realm only if it needs it ( we aren't changing realms in real time ... or? )
//
function autoloader:SetFileRealm( _folder, _file, _realm )
	-- self:InitFolders( _folder, _file );
	self.cache.data[ _folder ].files[ _file ].realm = _realm || REALM_UNKNOWN;
end


//
// Updates the file-time of a given file
//
function autoloader:UpdateFileTime( _folder, _file )
	if ( SERVER && _folder && _file ) then
		self.cache.data[ _folder ].files[ _file ].time = file.Time( self:GetBaseFolder( ) .. _folder .. _file, self:GetBasePath( ) );
	end
end


//
// Returns the file-time of a given file
//
function autoloader:GetFileTime( _folder, _file )
	return self.cache.data[ _folder ].files[ _file ].time, file.Time( self:GetBaseFolder( ) .. _folder .. _file, self:GetBasePath( ) );
end


//
// Calculates the realm of a given folder
//
function autoloader:BuildFolderRealm( _folder )
	// Otherwise we need to calculate it, then store it ( in each folder which doesn't have one )
	local _base = "";
	local _calculated_realm = REALM_UNKNOWN;
	local _parent_realm = REALM_UNKNOWN;
	local _realm = REALM_UNKNOWN;
	for k, v in pairs( string.Explode( "/", _folder ) ) do
		// Sorry; all folders should be lowercase...
		v = string.Trim( string.lower( v ) );

		// Make sure the folder isn't blank
		if ( v == "" ) then continue; end

		// Save the parent from getting wasted
		local _parent = _base;

		// The current folder
		_base = _base .. v .. "/";

		// Initialize the current folder table...
		-- self:InitFolders( _base );

		// Grab the parent realm, just in case we need it
		_parent_realm = self:GetFolderRealm( _parent );

		// Grab current folder realm in case it exists ( probably doesn't ), if not use parent...
		_realm = self:GetFolderRealm( _base ) || _parent_realm;

		// Then, check to see if this folder is special ( realm changer ), otherwise use realm...
		_realm = self.runtime.folder_realms[ v ] || _realm;

		// If the realm is set
		if ( _realm ) then
			// Then update the folder realm...
			self:SetFolderRealm( _base, _realm );
		end
	end

	return _realm;
end


//
// Prefix system - instead of parsing the string many times over for each 2, 3, 4 char prefix and 2, 3 char postfixes
// just generate them once and reference the values when needed.
//
function autoloader:GeneratePreAndPostFixes( _file )
	return {
		pre = {
			""; -- No need for 1
			string.sub( _file, 1, 2 ); // XXre_filename.lua
			string.sub( _file, 1, 3 ); // XXXe_filename.lua
			string.sub( _file, 1, 4 ); // XXXX_filename.lua
		};
		post = {
			""; -- No need for 1
			string.sub( _file, -6, -5 ); // _pre_filenaXX.lua
			string.sub( _file, -7, -5 ); // _pre_filenXXX.lua
		};
	};
end


//
// Checks to see if a file has pre/postfix
//
function autoloader:IsPrefix( _file, _chars, _string, _data )
	return ( _chars > 0 ) && _data.pre[ _chars ] == _string || _data.post[ math.abs( _chars ) ] == _string;
end


//
// Calculates the realm of a file
//
function autoloader:BuildFileRealm( _folder, _file )

	local _data = self:GeneratePreAndPostFixes( _file );

	// x files skip loading, non lua files skip loading.
	if ( self:IsPrefix( _file, 2, "x.", _data ) || self:IsPrefix( _file, 2, "x_", _data ) || self:IsPrefix( _file, 3, "_x_", _data ) || self:IsPrefix( _file, -2, "_x", _data ) || self:IsPrefix( _file, -2, ".x", _data ) || string.GetExtensionFromFilename( _file ) != "lua" ) then
		return REALM_NOLOAD;
	end

	// Check for cl_, _cl_, _cl.lua, sh_, _sh_, _sh.lua, sv_, _sv_, _sv.lua for specific loading realm...
	local _bFileStartsCL = ( _file == "client.lua" || self:IsPrefix( _file, 3, "cl.", _data ) || self:IsPrefix( _file, 3, "cl_", _data ) || self:IsPrefix( _file, 4, "_cl_", _data ) || self:IsPrefix( _file, -3, "_cl", _data ) || self:IsPrefix( _file, -3, ".cl", _data ) );
	local _bFileStartsSH = ( _file == "shared.lua" || self:IsPrefix( _file, 3, "sh.", _data ) || self:IsPrefix( _file, 3, "sh_", _data ) || self:IsPrefix( _file, 4, "_sh_", _data ) || self:IsPrefix( _file, -3, "_sh", _data ) || self:IsPrefix( _file, -3, ".sh", _data ) );
	local _bFileStartsSV = ( _file == "init.lua" || self:IsPrefix( _file, 3, "sv.", _data ) || self:IsPrefix( _file, 3, "sv_", _data ) || self:IsPrefix( _file, 4, "_sv_", _data ) || self:IsPrefix( _file, -3, "_sv", _data ) || self:IsPrefix( _file, -3, ".sv", _data ) );

	// Update the realm for the include
	local _realm = self:GetFolderRealm( _folder );
	_realm = ( _bFileStartsCL ) && REALM_CLIENT || _realm;
	_realm = ( _bFileStartsSH ) && REALM_SHARED || _realm;
	_realm = ( _bFileStartsSV ) && REALM_SERVER || _realm;

	return _realm;
end


//
// Helper function to set up ProcessLater points...
//
function autoloader:ProcessLater( _folder, _realm, _callback )
	local function HandleProcessLater( )
		self:AddFolder( _folder, _realm, _callback );
	end

	hook.Add( "InitPostEntity", "ProcessLater:InitPostEntity:" .. _folder, HandleProcessLater );
	hook.Add( "OnReloaded", "ProcessLater:OnReloaded:" .. _folder, HandleProcessLater );
end


//
//
//
function autoloader:HandleFileInclusions( _folder, _files )
	// Update Working Folder
	self:SetWorkingFolder( self:GetBaseFolder( ) .. _folder );

	// Process files to be categorized / loaded, etc...
	if ( istable( _files ) ) then
		for k, v in pairs( _files ) do
			local _file = string.Trim( string.lower( v ) );
			local _ext = string.GetExtensionFromFilename( _file );
			if ( _ext != "lua" ) then continue; end

			self:IncludeFile( _folder, _file );
		end
	end
end


//
//
//
function autoloader:HandleFolderInclusions( _folder, _folders )
	// Process folders
	if ( istable( _folders ) ) then
		for k, v in pairs( _folders ) do
			local _dir = string.Trim( string.lower( v ) );

			// Check to see if the current folder is a "load later" folder.
			local _process_later_func = self.runtime.process_later[ _dir ];
			if ( _process_later_func ) then
				// If it is, grab the generated folder-name from the function we call and the default realm...
				local _vfolder, _realm = _process_later_func( _folder, _dir );

				// Call a helper-function which creates the hooks...
				self:ProcessLater( _vfolder, _realm, _callback );
			else
				// Add each folder so we can process each folder, leave realm nil for it to be calculated...
				self:AddFolder( _folder .. _dir .. "/", nil, _callback );
			end
		end
	end
end


//
// Adds a folder to process...
//
function autoloader:AddFolder( _folder, _realm, _callback )
	// If the user defined a realm for this folder set, otherwise build...
	local _folder_realm = self:GetFolderRealm( _folder );
	if ( !_folder_realm && _realm ) then
		self:SetFolderRealm( _folder, _realm );
	elseif ( !_folder_realm ) then
		self:BuildFolderRealm( _folder );
	end

	// Grab all files and folders for the current folder we're in...
	local _files, _folders = file.Find( self:GetBaseFolder( ) .. _folder .. "*", self:GetBasePath( ) );

	// Should files be loaded deepest-folder first, or shallow-first?
	if ( self.config.deep_first_include ) then
		self:HandleFolderInclusions( _folder, _folders );
		self:HandleFileInclusions( _folder, _files );
	else
		self:HandleFileInclusions( _folder, _files );
		self:HandleFolderInclusions( _folder, _folders );
	end
end


//
//
//
function autoloader:InternalInclude( _file, _realm )
	// AddCSLuaFile if this is the server and the file needs to be downloaded by the client
	if ( SERVER && ( _realm == REALM_CLIENT || _realm == REALM_SHARED ) ) then
		AddCSLuaFile( _file );
	end

	// Include the file if it is shared and meant for either client or server, or it is either client or server file and in respective realm
	if ( _realm == REALM_SHARED || ( SERVER && _realm == REALM_SERVER ) || ( CLIENT && _realm == REALM_CLIENT ) ) then
		include( _file );
	end
end


//
// Includes a given file
//
local _file_inclusion_output = "";
function autoloader:IncludeFile( _folder, _file, _override_realm, _useCurrentDir )
	local _base = ( isstring( _useCurrentDir ) ) && _useCurrentDir || ( ( isbool( _useCurrentDir ) ) && self:GetWorkingFolder( ) || self:GetBaseFolder( ) );

	// Prevent loading the same file twice
	if ( self.runtime.current_process.files[ _base .. _folder .. _file ] ) then return false; end
	self.runtime.current_process.files[ _base .. _folder .. _file ] = true;

	// Initialize folder if needed.
	-- self:InitFolders( _folder, _file );

	// Grab cached folder realm, calculate if need be
	local _folder_realm = self:GetFolderRealm( _folder );
	if ( !_folder_realm || _folder_realm == REALM_UNKNOWN ) then
		_folder_realm = self:BuildFolderRealm( _folder );
	end

	// Grab cached file realm or calculate if need be
	local _file_realm = self:GetFileRealm( _folder, _file );
	if ( !_file_realm || _file_realm == REALM_UNKNOWN ) then
		_file_realm = self:BuildFileRealm( _folder, _file );
	end

	// Smart Auto-Refresh System
	local _bInSmartRefresh = self.config.smart_refresh;
	local _cached_time, _current_time = 0, 0;
	local _bHasChanged = false;

	// Preliminary Smart Auto-Refresh check; If enabled, then we'll only update the file iff file-time is newer than cached file-time
	if ( SERVER && _bInSmartRefresh ) then
		// Grab the cached file-time and the current file-time
		_cached_time, _current_time = self:GetFileTime( _folder, _file );

		// If our file has changed, alter our helper var; if it hasn't then return; to prevent files from loading...
		if ( _current_time > _cached_time ) then
			_bHasChanged = true;

			// Add the file to a list just in case each file needs to be sent to the client...
			self.cache.files_updated[ _folder .. _fole ] = _current_time;
		else
			return true;
		end
	end

	// Use the most logical realm ( file first otherwise folder, allow allow override for require calls )
	local _realm = ( _file_realm != REALM_UNKNOWN ) && _file_realm || _folder_realm;
	_realm = _override_realm || _realm;

	// Count the files...
	self:UpdateRealmFileCount( _realm );

	// If the realm ends up being unknown, don't do anything with the file ( happens on txt files, etc. we don't want them anyway )
	if ( _realm == REALM_UNKNOWN || _realm == REALM_NOLOAD ) then return false; end

	// We want to AddCSLuaFile / include even during smart-refresh to hopefully update the string-tables ( so new
	// users connecting get latest file without needing to manually send them )
	self:InternalInclude( _base .. _folder .. _file, _realm );


	// Smart Auto-Refresh System to resend files iff smart enabled and the file changed
	if ( SERVER && _bInSmartRefresh && _bHasChanged ) then
		// Grab the data from the file because this is an updated file
		local _data = file.Read( _base .. _folder .. _file, self:GetBasePath( ) );

		// Handle Client Include
		if ( _realm == REALM_CLIENT || _realm == REALM_SHARED ) then
			// Only need to network if no fix is found for file.Time; but this still doesn't even resolve
			// the issue if a cl_ file is updated... OnReloaded won't fire for client refreshes on the server.
			networking:Broadcast( "FileRefreshed", _gmfolder, _folder, _file, _realm, _data );
		end

		// Handle Server Include
		if ( _realm == REALM_SERVER || _realm == REALM_SHARED ) then
			RunString( _data || "" );
		end

		self:UpdateFileTime( _folder, _file );
	end

	// If we need to save the inclusion list to a file for comparisons...
	if ( self.config.file_inclusions ) then
		_file_inclusion_output = _file_inclusion_output .. REALMS[ _realm ] .. ": " .. _base .. _folder .. _file .. "\n";
		// Create a timer so the auto-includer will finish prior to saving
		timer.Create( "SaveFileInclusions", 1, 1, function( )
			file.Write( "acecooldev/gm_files/load_order.txt", _file_inclusion_output );
		end );
	end

	// Output files that get loaded in exact load-order if output_inclusions is set...
	if ( self.config.output_inclusions ) then
		print( REALMS[ _realm ] .. ": " .. _base .. _folder .. _file );
	end
end


//
// Simple solution to get the parent folder...
//
function autoloader:ParentFolder( _folder )
	// First we make sure this is a valid path.. ie no filename attached by using a built in function.
	local _folder = string.GetPathFromFilename( _folder );

	// Now we trim the last character ( a / ) so when we run the same function above again, it'll strip it..
	_folder = string.sub( _folder, 1, -2 );

	// Strip the folder-name now that the forward-slash doth not protect thee
	_folder = string.GetPathFromFilename( _folder );

	// Return the parent directory...
	return _folder;
end


//
// For each ../ we shave a directory / step off of our working / base dir until nothing is left of _base or no ../s remain
//
function autoloader:HandleUpStepping( _base, _folder )
	// Logic which allows ../ to be used by shaving off our base folder to step up...
	local _bHasUpDir = true;
	local _steps_count = 0;

	// Simple Algorithm for shaving data
	while true do
		// Determine whether or not we need to be here. This method assumes ALL ../ are at the beginning of the filename.
		_bHasUpDir = ( string.StartWith( _folder, "../" ) );

		// For every recursive or potentially infinite loop we need to define our exit strategy...
		if ( !_bHasUpDir || string.len( _base ) == 0 ) then break; end

		// Remove the ../ from the folder name
		_folder = string.sub( _folder, 4 );

		// Get the parent folder from our base / working dir side
		_base = self:ParentFolder( _base );

		// Increase steps
		_steps_count = _steps_count + 1;
	end

	// Return the new or old base, folder and steps we've taken
	return _base, _folder, _steps_count;
end


//
// Includes a file if it hasn't yet been included
//
function require_once( _file, _realm, _useCurrentDir )
	// Add basic protection so clients or server don't get "can't find file" errors for files not meant for them...
	if ( ( CLIENT && _realm == REALM_SERVER ) || ( SERVER && _realm == REALM_CLIENT ) ) then return; end

	// Simple helpers
	local self = autoloader;
	local _base = ( _useCurrentDir ) && self:GetWorkingFolder( ) || self:GetBaseFolder( );

	local _folder = string.GetPathFromFilename( _file ) || "";
	local _new_file = string.GetFileFromFilename( _file ) || _file;
	_file = ( _new_file == "" ) && _file || _new_file;

	// Logic which allows ../ to be used by shaving off our base folder to step up...
	_base, _folder, _steps_count = self:HandleUpStepping( _base, _folder );

	local _exists = file.Exists( _base .. _folder .. _file, self:GetBasePath( ) );

	if ( _exists ) then
		self:IncludeFile( _folder, _file, _realm, ( ( _useCurrentDir && _steps_count > 0 ) && _base || _useCurrentDir ) );
	else
		MsgC( Color( 255, 0, 0, 255 ), "require_once couldn't locate file: ", color_white, _base .. _folder .. _file .. "\n" );
	end
end


//
//
//
function autoloader:__OutputFileCounts( _realm, ... )
	local _counters = self.runtime.current_process.file_count;
	local _output = ( _counters[ _realm ] > 0 ) && _counters[ _realm ] .. " " .. REALMS[ _realm ] .. "" || "";
	local _comma = ( _output != "" ) && ", " || "";

	if ( #{ ... } > 0 ) then _output =	_output .. _comma .. self:__OutputFileCounts( ... ); end
	if ( string.EndsWith( _output, ", " ) ) then _output = "and " .. string.sub( _output, 1, -3 ) .. " files..."; end

	return _output;
end


//
// Override function - use hook.Add to add all folders / files to include...
//
function autoloader:SetupAutoLoader( )

end


//
// Override function - use hook.Add to make things happen after all files have loaded
//
function autoloader:PostAutoLoader( )

end


//
//
//
function autoloader:LoadAddon( )
	// Reset the "current process" which contains a cache of loaded files ( to prevent loading the same one twice )
	autoloader:Reset( );

	// Best Load-Order. Best is SHARED, Client/Server, Addons, Content, etc...

	// Game-Mode Auto-Loader callback; this is so one auto-loader can control all addons too
	hook.Call( "SetupAutoLoader", autoloader, self );

	// Setup AcecoolDev_Networking Addon
	// Location: addons/<addon_name>/lua/acecooldev_networking/
	self:SetBaseFolder( "acecooldev_networking/", "LUA" );	-- Second arg is optional, lets you set lsv path with path..
	self:AddFolder( "definitions/", REALM_SHARED );			-- Adding this for Linux users ( _ comes after a-z )
	self:AddFolder( "core/", REALM_SHARED );				-- Adding this for Linux users ( _ comes after a-z )
	self:AddFolder( "core_meta/", REALM_SHARED );			-- Adding this for Linux users ( _ comes after a-z )
	self:AddFolder( "shared/", REALM_SHARED );
	self:AddFolder( "client/", REALM_CLIENT );
	self:AddFolder( "server/", REALM_SERVER );
	self:AddFolder( "addons/", REALM_SHARED );

	// Show loaded file-count if enabled or first time game loads
	if ( !AUTOLOADER_LOADED_ADDON || self.config.output_load_info ) then
		MsgC( color_white, "AutoLoader Processed " .. self:__OutputFileCounts( REALM_CLIENT, REALM_SHARED, REALM_SERVER, REALM_NOLOAD, REALM_UNKNOWN ) .. "\n" );
	end

	// ...
	hook.Call( "PostAutoLoader", autoloader, self );

	// Some tracking data...
	AUTOLOADER_LOADED_ADDON			= true;
	AUTOLOADER_LOADED_ADDON_TIME	= os.time( );
end


//
// Call the Auto-Loader; this is here for initial load and auto-refresh; for disabled auto-refresh / Linux
// users, see loadaddon console command...
//
-- hook.Add( "Initialize", "AddonAutoloaderOnInitialize", function( )
	autoloader:LoadAddon( );
-- end );


//
// Fake Auto-Refresh to get by the annoyance of saving multiple files for new files to show up...
//
if ( SERVER ) then
	//
	// Server=side Forced Auto-Refresh console command: loadaddon
	//
	concommand.Add( "loadaddon", function( _p, _cmd, _args )
		// Make sure Player is NULL or IsSuperAdmin, otherwise STOP. NULL player is RCON user...
		if ( _p != NULL && !_p:IsSuperAdmin( ) ) then return; end

		// Server-Side refresh
		GM = GAMEMODE;
		autoloader:LoadAddon( );
		hook.Call( "OnReloaded", GAMEMODE );

		// Client-Side refresh
		BroadcastLua( "GM = GAMEMODE; autoloader:LoadAddon( ); hook.Call( \"OnReloaded\", GAMEMODE );" );
	end );
end